export function signify(value: number, zeroIsPositive = true): string {
  let sign = ""

  if (value == 0 && zeroIsPositive) {
    sign = "+"
  } else if (value > 0) {
    sign = "+"
  } else if (value < 0) {
    sign = "-"
  }

  return `${sign}${Math.abs(value)}`

}

import {Pair} from "./pair";

export function assignString(val: any, className: string, propName: string): string {
  return assignFromType(val, "string", "string", className, propName);
}

export function assignEnumByValue(val: any, className: string, propName: string, validValues: any[]): number {
  const value: number = assignFromType(val, "number", "number", className, propName) as number;
  const index = validValues.filter(key => !isNaN(Number(key))).findIndex(x => x === value);
  if (index > -1) {
    return validValues[index];
  }
  throw new Error(
    `Error Reading ${className}: ${propName} = ${val}; is not in allowed list: {${validValues.join(", ")}}`);
}

export function assignEnumByName(val: any, className: string, propName: string, validValues: any[]): number {
  const value: string = assignFromType(val, "string", "string", className, propName) as string;
  const index = validValues.findIndex(x => x === value);
  if (index > -1) {
    return validValues[index + validValues.length / 2];
  }
  throw new Error(
    `Error Reading ${className}: ${propName} = ${val}; is not in allowed list: {${validValues.join(", ")}}`);
}

export function assignStringEnum(val: any, className: string, propName: string, validValues: any[]): string {
  const value: string = assignFromType(val, "string", "строкой", className, propName) as string;
  if (validValues.findIndex(x => x === value) > -1) {
    return value;
  }
  throw new Error(
    `Error Reading ${className}: ${propName} = ${val}; is not in allowed list:  {${validValues.join(", ")}}`);
}

export function assignNumber(val: any, className: string, propName: string): number {
  return assignFromType(val, "number", "number", className, propName);
}

export function assignBoolean(val: any, className: string, propName: string): boolean {
  return assignFromType(val, "boolean", "boolean", className, propName);
}

export function checkIsArray<T>(val: any, className: string, propName: string): any[] {
  if (!(val && Array.isArray(val))) {
    throw new Error(`Error Reading ${className}: ${propName} = ${val}, it's not an array`);
  }

  return val;
}

export function assignFromType(val: any, typeName: string, typeDesc: string, className: string, propName: string): any {
  if (typeof (val) !== "undefined" && (val == null || typeof val === typeName)) {
    return val;
  }
  throw new Error(`Error Reading ${className}: ${propName} = ${val}, it's not ${typeDesc}`);
}

export class AssignsMapping {
  constructor(
    public prop: string,
    public converter: ((propVal: any, srcObj: any, propName: string, className: string) => any) | null = null,
    public ignore: boolean = false,
    public forceIfUndefined: boolean = false
  ) {
  }
}

export function mapMeta(target: any): AssignsMapping[] {
  const mappings: AssignsMapping[] = Object.keys(target).map(p => new AssignsMapping(p))
  return mappings
}

export function mapObjects(src: any, tgt: any, props: AssignsMapping[], caption: string) {

  props.forEach(p => {

    if (!p.ignore) {

      if (tgt[p.prop] == undefined && !p.forceIfUndefined) {
        throw Error(`Can't map object ${caption}: property ${p.prop} is undefined in target`)
      }

      if (p.converter) {
        tgt[p.prop] = p.converter(src[p.prop], src, caption, p.prop) // assign by lambda
      } else {  // assign by value
        if (src[p.prop] == undefined) {
          throw Error(`Can't map object ${caption}: property ${p.prop} is undefined in source`)
        }

        switch (typeof tgt[p.prop]) {
          case "object":
            //safe check arrays
            const ta = Array.isArray(tgt[p.prop])
            const sa = Array.isArray(src[p.prop])

            if (ta !== sa) {
              throw Error(`Can't map object ${caption}: property ${p.prop} should be either array on both sides on no side. Target: ${ta}, Source: ${sa}`)
            } else if (ta === true) {
              //both are arrays
              tgt[p.prop] = src[p.prop]
            } else {
              // other objects, do our best
              if (tgt[p.prop] instanceof src[p.prop]) {
                tgt[p.prop] = src[p.prop]
              } else {
                throw Error(`Can't map object ${caption}: property ${p.prop} is complex and doesn't match source`)
              }
            }

            break
          case "boolean":
            tgt[p.prop] = assignBoolean(src[p.prop], caption, p.prop);
            break
          case "number":
            tgt[p.prop] = assignNumber(src[p.prop], caption, p.prop);
            break
          case "string":
            tgt[p.prop] = assignString(src[p.prop], caption, p.prop);
            break
          default:
            throw Error(`Can't map object ${caption}: property ${p.prop} type is of unmappable type '${typeof tgt[p.prop]}'`)
        }
      }
    }
  })
}

export function pairsArrayToMap<V>(arr: Pair<string, V>[]): Map<string, V> {
  let map = new Map<string, V>()
  arr.forEach(e => {
    map.set(e.key, e.value)
  })

  return map
}

export function mapToPairsArray(map: Map<string, any>): Pair<string, any>[] {
  let records: Pair<string, any>[] = []

  map.forEach((v, k) => {
    records.push({key: k, value: v})
  })

  return records
}

import {AbstractControl, ValidationErrors} from "@angular/forms";

export class ValidatePassword {
  static matchPasswords(abstractControl: AbstractControl): ValidationErrors | null {
    const password = abstractControl.get("password")?.value;
    const confirmPassword = abstractControl.get("confirmPassword")?.value;
    if (password !== confirmPassword) {
      abstractControl.get("confirmPassword")?.setErrors({
        MatchPassword: true
      });
      return ({not_the_same: true});
    } else {
      return null;
    }
  }
}

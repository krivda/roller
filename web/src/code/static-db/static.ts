//Setting data. TODO - load from db
export let skillsDB = { //id: [name, type, attr, default, category]

  "Acrobatic": ["Acrobatic", "PH", "DX", -5, "Athletic"],
  "BreathControl": ["Breath Control", "MV", "IQ", -5, "Athletic"],
  "LowGFight": ["Low-G Fight", "PA", "DX", -5, "Athletic"],
  "FreeFall": ["Free Fall", "PA", "DX", -4, "Athletic"],
  "Parachuting": ["Parachuting", "PE", "DX", -4, "Athletic"],
//'Swimming':['Swimming','PE', 'DX', -4, 'Athletic'],
  "Throwing": ["Throwing", "PH", "DX", -4, "Athletic"],
  "Running": ["Running", "PH", "ST", -4, "Athletic"],

  "Battlesuit": ["Battlesuit", "PA", "DX", -4, "Combat"],
  "Brawling": ["Brawling", "PE", "DX", -4, "Combat"],
  "FastDraw": ["Fast-Draw", "PE", "DX", -4, "Combat"],
  "Gunner": ["Gunner", "PA", "DX", -4, "Combat"],
  "Guns": ["Guns", "PE", "DX", -4, "Combat"],
  "GuidedMissiles": ["Guided Missiles", "ME", "IQ", -4, "Combat"],
  "Knife": ["Knife", "PE", "DX", -4, "Combat"],
  "Karate": ["Karate", "PH", "DX", -4, "Combat"],

  "Mechanic": ["Mechanic", "MA", "IQ", -4, "Craft"],
  "Chess": ["Chess", "ME", "IQ", -4, "Hobby"],
  "PrecursorLanguage": ["Precursor Language", "MH", "IQ", -4, "Language"],//custom

  "FirstAid": ["First Aid", "ME", "IQ", -4, "Medical"],
  "Doctor": ["Doctor", "MH", "IQ", -4, "Medical"],
  "Diagnosis": ["Diagnosis", "MH", "IQ", -4, "Medical"],
  "Surgery": ["Surgery", "MV", "IQ", -4, "Medical"],

  "DroneOperation": ["Drone Operation", "MA", "IQ", -4, "Military"],//custom
  "Tactics": ["Tactics", "MH", "IQ", -4, "Military"],
  "NoLandingExtraction": ["No-Landing Extraction", "MA", "IQ", -4, "Military"],

  "Climbing": ["Climbing", "PA", "DX", -4, "Outdoor"],
  "SurvivalDesert": ["Survival Desert", "MH", "IQ", -4, "Outdoor"],
  "SurvivalSpace": ["Survival Space", "MH", "IQ", -4, "Outdoor"],
  "SurvivalTombword": ["Survival Tombword", "MV", "IQ", -4, "Outdoor"],

  "ComputerOperation": ["Computer Operation", "ME", "IQ", -4, "Professional"],
  "ElectronicOperation": ["Electronic Operation", "MA", "IQ", -4, "Professional"],
  "Investigation": ["Investigation", "MH", "IQ", -4, "Professional"],
  "ComputerProgramming": ["Computer Programming", "MH", "IQ", -4, "Professional"],
  "ComputerHacking": ["Computer Hacking", "MH", "IQ", -4, "Professional"],
  "Electronics": ["Electronics", "MH", "IQ", -4, "Professional"],
  "Engineer": ["Engineer", "MH", "IQ", -4, "Professional"],
  "Orbital Mechanics": ["Orbital Mechanics", "MA", "IQ", -4, "Professional"],//custom

  "Antropology": ["Antropology", "MH", "IQ", -4, "Scientific"],
  "Astronomy": ["Astronomy", "MH", "IQ", -4, "Scientific"],
  "Bilology": ["Bilology", "MH", "IQ", -4, "Scientific"],
  "Chemistry": ["Chemistry", "MH", "IQ", -4, "Scientific"],
  "Geology": ["Geology", "MH", "IQ", -4, "Scientific"],
  "Archeology": ["Archeology", "MH", "IQ", -4, "Scientific"],
  "PrecursorArcheology": ["Precursor Archeology", "MV", "IQ", -4, "Scientific"],
  "Cryptorgaphy": ["Cryptorgaphy", "MH", "IQ", -4, "Scientific"],
  "Fornestics": ["Fornestics", "MH", "IQ", -4, "Scientific"],
  "Genetics": ["Genetics", "MH", "IQ", -4, "Scientific"],
  "Mathematics": ["Mathematics", "MH", "IQ", -4, "Scientific"],
  "Philosophy": ["Philosophy", "MH", "IQ", -6, "Scientific"],
  "Planetology": ["Planetology", "MH", "IQ", -4, "Scientific"],
  "Psychology": ["Psychology", "MH", "IQ", -4, "Scientific"],
  "Physics": ["Physics", "MH", "IQ", -4, "Scientific"],
  "Theology": ["Theology", "MH", "IQ", -4, "Scientific"],

  "Driving": ["Driving", "PA", "DX", -4, "Vechicle"],
  "Exoskeleton": ["Exoskeleton", "PA", "DX", -4, "Vechicle"],
  "PilotingAtmosphere": ["Piloting Atmosphere", "PA", "DX", -4, "Vechicle"],
  "PilotingSpace": ["Piloting Space", "PA", "DX", -4, "Vechicle"],
  "VaccSuit": ["Vacc Suit", "MA", "IQ", -4, "Vechicle"],

  "Demolition": ["Demolition", "MA", "IQ", -4, "Thief/Spy"],
  "DetectLies": ["Detect Lies ", "MH", "IQ", -4, "Thief/Spy"],
  "Disguise": ["Disguise", "MA", "IQ", -4, "Thief/Spy"],
  "HoldoutConcealment": ["Holdout/Concealment", "MA", "IQ", -4, "Thief/Spy"],
  "Interrogation": ["Interrogation", "MA", "IQ", -4, "Thief/Spy"],
  "LipReading": ["Lip Reading", "MA", "IQ", -4, "Thief/Spy"],
  "Pickpocket": ["Pickpocket", "PH", "DX", -4, "Thief/Spy"],
  "Poisons": ["Poisons", "MH", "IQ", -4, "Thief/Spy"],
  "Security": ["Security", "MA", "IQ", -4, "Thief/Spy"],
  "Traps": ["Traps", "MA", "IQ", -4, "Thief/Spy"],
  "Shadowing ": ["Shadowing", "MA", "IQ", -4, "Thief/Spy"],
  "Stealth": ["Stealth", "PA", "DX", -4, "Thief/Spy"],
  "Streetwise": ["Streetwise", "MA", "IQ", -4, "Thief/Spy"],

  "Administration": ["Administration", "MA", "IQ", -4, "Social"],
  "Charm": ["Charm", "MH", "IQ", -4, "Social"],//custom
  "Diplomacy": ["Diplomacy", "MH", "IQ", -4, "Social"],
  "Performance": ["Performance", "MA", "IQ", -4, "Social"],
  "Leadership": ["Leadership", "MA", "IQ", -4, "Social"],
  "Manipulation": ["Manipulation", "MH", "IQ", -4, "Social"], //custom
  "SavoirFaire": ["Savoir-Faire", "ME", "IQ", -4, "Social"],

  "Meditation": ["Meditation", "MA", "IQ", -6, "Psionic"],  //custom
  "PsiReadMind": ["Psi: Read Mind", "MA", "IQ", -6, "Psionic"],  //custom
  "PsiSenseMind": ["Psi: Sense Mind", "MA", "IQ", -6, "Psionic"],  //custom
  "PsiMentalSpeech": ["Psi: Mental Speech", "MA", "IQ", -6, "Psionic"],  //custom
  "PsiConfuseMind": ["Psi: Confuse Mind", "MH", "IQ", -6, "Psionic"],  //custom
  "PsiDistractMind": ["Psi: Distract Mind", "MH", "IQ", -6, "Psionic"],  //custom
  "PsiFear": ["Psi: Fear", "MVH", "IQ", -6, "Psionic"],  //custom
  "PsiSream": ["Psi: Sream", "MVH", "IQ", -6, "Psionic"],  //custom
  "PsiMassFear": ["Psi: Mass Fear", "MVH", "IQ", -6, "Psionic"],  //custom
  "PsiClosedMind": ["Psi: Closed Mind", "MH", "IQ", -6, "Psionic"],  //custom
  "PsiSuppress Will": ["Psi: Suppress Will", "MH", "IQ", -6, "Psionic"],  //custom
  "PsiEnlightenedMind": ["Psi: Enlightened Mind", "MVH", "IQ", -6, "Psionic"],  //custom
  "PsiCosmicConsciousness": ["Psi: Connect to the cosmic consciousness", "MVH", "IQ", -6, "Psionic"],  //custom
}

export let advDB = { //имя, цена, тип, максимум раз
  "Ambidexterity": ["Ambidexterity", 10, "Advantage", 1],
  "AttractiveAppearance": ["Attractive Appearance", 5, "Advantage", 3],
  "Charisma": ["Charisma", 5, "Advantage", 1],
  "CombatReflexes": ["Combat Reflexes", 15, "Advantage", 1],
  "CommonSense": ["Common Sense", 10, "Advantage", 1],
  "CryosleepTolerance": ["Cryosleep Tolerance", 10, "Advantage", 1],//custom
  "DangerSense": ["Danger Sense", 15, "Advantage", 1],
  "Empathy": ["Empathy", 15, "Advantage", 1],
  "ExtendedLifespan": ["Extended Lifespan", 5, "Advantage", 5],
  "EideticMemory": ["Eidetic Memory", 10, "Advantage", 2],
  "HighPainThreshold": ["High Pain Threshold", 10, "Advantage", 1],
  "HighGNative": ["High-G Native", 15, "Advantage", 1], //custom
  "Intuition": ["Intuition", 15, "Advantage", 1],
  "ManualDexterity": ["Manual Dexterity", 3, "Advantage", 2],
  "Implant": ["Implant", 5, "Advantage", 10],
  "Neuroadaptation": ["Neuroadaptation", 15, "Advantage", 1],//custom
  "RapidHealing": ["Rapid Healing", 5, "Advantage", 1],
  "Status": ["Status", 5, "Advantage", 5],
  "Stimtolerance": ["Stim tolerance", 10, "Advantage", 1],
  "Patron": ["Patron", 5, "Advantage", 5],
  "Reputation": ["Reputation", 5, "Advantage", 5],
  "Regeneration": ["Regeneration", 15, "Advantage", 1], //custom
  "PsionicTalent": ["Psionic Talent", 15, "Advantage", 5], //custom


  "Addiction": ["Addiction", -5, "Disadvantage", 5],
  "AbsentMindedness": ["Absent-Mindedness", -5, "Disadvantage", 5],
  "Berserk": ["Berserk", -15, "Disadvantage", 1],
  "Curious": ["Curious", -5, "Disadvantage", 1],
  "CompulsiveVowing": ["Compulsive Vowing", -5, "Disadvantage", 1],
  "Cowardice": ["Cowardice", -10, "Disadvantage", 1],
  "Cyberpsychosis": ["Cyberpsychosis", -5, "Disadvantage", 4],//custom
  "Duty": ["Duty", -5, "Disadvantage", 5],
  "Delusion": ["Delusion", -5, "Disadvantage", 4],
  "Dependence": ["Dependence", -5, "Disadvantage", 5],
  "EasyToRead": ["Easy To Read", -10, "Disadvantage", 1],
  "Enemy": ["Enemy", -5, "Disadvantage", 5],
  "Fanaticism": ["Fanaticism", -10, "Disadvantage", 1],
  "Flashbacks ": ["Flashbacks ", -10, "Disadvantage", 2],
  "GloryHound": ["Glory Hound", -15, "Disadvantage", 1],
  "Greed": ["Greed", -15, "Disadvantage", 1],
  "Hallucinations": ["Hallucinations", -10, "Disadvantage", 2],
  "Honesty": ["Honesty", -10, "Disadvantage", 1],
  "Intolerance": ["Intolerance", -5, "Disadvantage", 3],
  "Laziness": ["Laziness", -10, "Disadvantage", 1],
  "Lunacy": ["Lunacy", -10, "Disadvantage", 1],
  "Manic-Depressive": ["Manic-Depressive", -20, "Disadvantage", 1],
  "Nightmares": ["Nightmares", -5, "Disadvantage", 1],
  "Overconfidence": ["Overconfidence", -10, "Disadvantage", 1],
  "Panic attacks": ["Panic attacks", -15, "Disadvantage", 1],
  "Paranoia": ["Paranoia", -10, "Disadvantage", 1],
  "Phobia": ["Phobia", -5, "Disadvantage", 5],
  "PhobiaAI": ["Phobia: AI", -5, "Disadvantage", 5],
  "Cryophobia": ["Cryophobia", -5, "Disadvantage", 5],
  "Claustrophobia": ["Claustrophobia", -5, "Disadvantage", 5],
  "PhobiaZombie": ["Phobia: zombie", -5, "Disadvantage", 5],
  "Xenophobia": ["Xenophobia (mutants)", -5, "Disadvantage", 5],
  "Xenophobia_a": ["Xenophobia (aliens)", -5, "Disadvantage", 5],
  "PhobiaPsionics": ["Phobia: psionics", -5, "Disadvantage", 5],
  "PhobiaSpace": ["Phobia: being lost in space", -5, "Disadvantage", 5],
  "PhobiaVirus": ["Phobia: deadly virus", -5, "Disadvantage", 5],
  "Agoraphobia": ["Agoraphobia", -5, "Disadvantage", 5],
  "Pyrophobia": ["Pyrophobia", -5, "Disadvantage", 5],
  "Sadism": ["Sadism", -15, "Disadvantage", 1],
  "ShortLifespan": ["Short Lifespan", -10, "Disadvantage", 1],
  "Shyness": ["Shyness", -5, "Disadvantage", 3],

}

export let armorDB = { //name, PD, DR, HP, RR, mods: [statid, value]
  "noarmor": ["нет", 0, 0, 0, 0, []],
  "lightscaf": ["Легкий скафандр", 3, 6, 40, 2, []],
  "expscaf": ["Экспедиционный скафандр", 4, 5, 120, 5, [["Perception", -1]]],
  "protscaf": ["Защитный скафандр", 5, 20, 300, 6, [["DX", -1], ["Dodge", -2], ["Move", -2], ["Perception", -2], ["Stealth", -4]]],
  "infantry": ["Пехотный скафандр", 4, 15, 200, 3, [["Dodge", -1], ["Move", -1]]],
  "heavy": ["Тяжелый боевой скафандр", 6, 25, 500, 10, [["DX", -2], ["Dodge", -6], ["Stealth", -5], ["Guns", 2]]],
  "spacemarine": ["Десантный скафандр", 5, 15, 250, 5, [["Dodge", -1], ["Stealth", -1]]],
  "scout": ["Разведывательный скафандр", 4, 8, 80, 0, [["Stealth", 2]]],
  "termocomb": ["Термокомб", 4, 8, 60, 0, [["Stealth", 1]]],
  "radscaf": ["Противорадиационный скафандр", 5, 12, 150, 16, [["DX", -1], ["Dodge", -2], ["Move", -2], ["Perception", -1], ["Stealth", -4]]],
  "sandsuit": ["Джавшан", 4, 10, 100, 0, [["Stealth", 2]]],
}

export let weaponDB = { //name, type, dmg1,dmg2, ss, AC, ROF1,ROF2,Recoil,Range, Ammo, st
  "shocker": ["Шокер", "shock", 1, 1, 8, 1, 1, 0, 0, 5, 30, 6],
  "stunner": ["Станнер", "stun", 1, 0, 10, 3, 2, 0, 0, 40, 20, 5],
  "pneumogun": ["Пневморужье", "cr", 1, 0, 12, 6, 2, 0, 1, 250, 5, 7],
  "grenade_gun": ["Ручной гранатомет", "cr", 1, 0, 11, 3, 2, 0, 2, 100, 5, 8],
  "grenade_subgun": ["Подствольный гранатомет", "cr", 1, 0, 11, 3, 1, 0, 2, 100, 2, 8],
  "gauss_pistol": ["Гауссовый пистолет", "imp", 3, 3, 10, 4, 3, 4, 1, 2000, 48, 6],
  "gauss_rifle": ["Гауссовая винтовка", "imp", 5, 5, 12, 11, 4, 5, 1, 10000, 120, 10],
  "gauss_mgun": ["Гауссовый пулемет", "imp", 6, 6, 14, 8, 4, 8, 2, 14000, 640, 11],
  "gauss_sniper": ["Снайперская винтовка", "imp", 6, 8, 14, 13, 1, 5, 1, 16000, 75, 11],
  "laser_rifle": ["Лазерная винтовка", "imp", 4, 4, 11, 12, 1, 8, 0, 0, 1, 12],
  "gamma_rifle": ["Гамма лазер", "imp", 3, 3, 12, 12, 1, 8, 0, 0, 1, 13],
  "sandman_laser_rifle": ["Лазер пустынников", "imp", 5, 10, 12, 10, 2, 4, 0, 0, 8, 12],
}

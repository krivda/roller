import {AdvantageKind, AdvantageMeta, ArmorMeta, AttributeMeta, CounterKind, CounterMeta, PrimaryAttributeKeys, SecondaryAttributeMeta, SkillMeta, WeaponMeta} from "./gurps-model";
import {assignNumber, assignString, checkIsArray} from "../../lib/assigns";
import {CharModifier, ModKind} from "./char-modifier";

export class Gurps {
  static readonly SNAPSHOT_MODIFIER = -4;
  static readonly MAX_AIM_ROUNDS = 4;


  public attributes = new Map<string, AttributeMeta>()
  public secondaryAttributes = new Map<string, SecondaryAttributeMeta>()
  public counters = new Map<string, CounterMeta>()
  public advantages = new Map<string, AdvantageMeta>();

  public skills = new Map<string, SkillMeta>()
  public armors = new Map<string, ArmorMeta>()
  public weapons = new Map<string, WeaponMeta>()

  public static loadModel(json: any): Gurps {

    let obj = new Gurps()

    obj.attributes = Gurps.loadPrimaryAttributes();
    obj.secondaryAttributes = Gurps.loadSecondaryAttributes();
    obj.advantages = Gurps.loadAdvantages(json.advantages)

    obj.counters = Gurps.loadCounters(obj)
    obj.skills = Gurps.loadSkills(json.skills, obj)
    obj.armors = Gurps.loadArmors(json.armors, obj)
    obj.weapons = Gurps.loadWeapons(json.weapons)

    return obj
  }

  private static loadPrimaryAttributes(): Map<string, AttributeMeta> {
    const attrs = new Map<string, AttributeMeta>()

    PrimaryAttributeKeys.baseAttributes.forEach(a =>
      attrs.set(a, new AttributeMeta(a, a))
    )

    return attrs
  }

  private static loadSecondaryAttributes(): Map<string, SecondaryAttributeMeta> {

    let attrs = new Map<string, SecondaryAttributeMeta>()

    attrs.set("extrafatigue", new SecondaryAttributeMeta("extrafatigue", "extrafatigue"))
    attrs.set("extrahp", new SecondaryAttributeMeta("extrahp", "extrahp"))
    attrs.set("alertness", new SecondaryAttributeMeta("alertness", "alertness"))
    attrs.set("will", new SecondaryAttributeMeta("will", "will"))
    attrs.set("Age", new SecondaryAttributeMeta("Age", "Age"))
    attrs.set("unused_cp", new SecondaryAttributeMeta("unused_cp", "unused_cp"))
    attrs.set("cHP", new SecondaryAttributeMeta("cHP", "cHP", true))
    attrs.set("cPsiEnergy", new SecondaryAttributeMeta("cPsiEnergy", "cPsiEnergy", true))
    attrs.set("cFatigue", new SecondaryAttributeMeta("cFatigue", "cFatigue", true))

    return attrs
  }

  private static loadSkills(json: any, model: Gurps): Map<string, SkillMeta> {

    let skillMap = new Map<string, SkillMeta>()

    //'Acrobatic':['Acrobatic','PH', 'DX', -5, 'Athletic'],

    const cls = "Skill"

    if (json) {

      for (const [key, raw] of Object.entries(json)) {
        let value: number[] = raw as number[]

        checkIsArray(value, cls, "skillValue")

        let skill = new SkillMeta(key,
          assignString(value[0], cls, "name"),
          assignString(value[1], cls, "hardness"),
          model.attributes.get(PrimaryAttributeKeys.parse(value[2]))!,
          assignNumber(value[3], cls, "default")
        )

        skillMap.set(key, skill)
      }
    } else {
      throw new Error(`Raw json for ${cls} is empty`)
    }

    return skillMap;
  }

  private static loadAdvantages(json: any): Map<string, AdvantageMeta> {
    let advMap = new Map<string, AdvantageMeta>()

    //'AttractiveAppearance':['Attractive Appearance', 5, 'Advantage', 3],

    const cls = "Advantage"
    if (json) {

      for (const [key, raw] of Object.entries(json)) {
        let value: number[] = raw as number[]

        checkIsArray(value, cls, "advantageValue")

        let kindStr = assignString(value[2], cls, "[2]/kind")
        let kind: AdvantageKind
        if (kindStr === "Advantage") {
          kind = AdvantageKind.advantage
        } else if (kindStr === "Disadvantage") {
          kind = AdvantageKind.disadvantage
        } else {
          throw new Error(`Can parse advantage kind for '${key} from '${kindStr}`)
        }

        let advantage = new AdvantageMeta(key,
          assignString(value[0], cls, "[0]/name"),
          kind,
          assignNumber(value[1], cls, "[1]/cost"),
          assignNumber(value[3], cls, "[3]/max")
        )

        if (key === "CombatReflexes") {
          advantage.modifiers.push(new CharModifier(ModKind.counter, "Dodge", "Combat Reflexes", "advantages", 1))
          advantage.modifiers.push(new CharModifier(ModKind.counter, "Block", "Combat Reflexes", "advantages", 1))
        }

        advMap.set(key, advantage)
      }
    } else {
      throw new Error(`Raw json for ${cls} is empty`)
    }

    return advMap;
  }


  private static loadArmors(json: any, model: Gurps): Map<string, ArmorMeta> {

    //name, PD, DR, HP, RR, mods: [statid, value]
    // 'protscaf':['Защитный скафандр',5,20,300,6,[['DX', -1], ['Dodge', -2], ['Move', -2], ['Perception', -2], ['Stealth', -4]]],
    let armorMap = new Map<string, ArmorMeta>()
    const cls = "Armor"

    if (json) {
      for (const [key, raw] of Object.entries(json)) {
        let value: number[] = raw as number[]
        checkIsArray(value, cls, "armorValueArray")

        let armor = new ArmorMeta(key,
          assignString(value[0], cls, "[0]/name"),
          assignNumber(value[1], cls, "[1]/PD"),
          assignNumber(value[2], cls, "[2]/DR"),
          assignNumber(value[4], cls, "[4]/RR"),
          assignNumber(value[3], cls, "[3]/HP"),
          Gurps.loadArmorModifiers(value[0], value[5], model)
        )

        armorMap.set(key, armor)
      }
    } else {
      throw new Error(`Raw json for ${cls} is empty`)
    }

    return armorMap;
  }

  private static loadArmorModifiers(name: any, modArray: any, model: Gurps): CharModifier[] {

    // [['DX', -1], ['Dodge', -2], ['Move', -2], ['Perception', -2], ['Stealth', -4]]]
    let cls = "Armor/mods"
    let arr = checkIsArray(modArray, "Armor", "armorMod[5]/mods")
    let modifiers: CharModifier[] = []

    arr.forEach(mod => {
      let subject = assignString(mod[0], cls, "[0]/subject")
      let value = assignNumber(mod[1], cls, "[1]/value")
      let kind = CharModifier.getKind(subject, model)

      if (kind == ModKind.other) {
        if (kind == ModKind.other) {
          throw Error(`Armor ${name} modifier '${subject}' cannot be bound to either skill, counter, of attribute!`)
        }
      }

      modifiers.push(new CharModifier(kind, subject, name, CharModifier.SRC_ARMOR, value, false, true))
    })

    return modifiers
  }


  private static loadWeapons(json: any): Map<string, WeaponMeta> {

    //name, type, dmg1,dmg2, ss, AC, ROF1,ROF2,Recoil,Range, Ammo, st
    //'shocker':['Шокер','shock',1,1,8,1,1,0,0,5,30,6],

    let weaponsMap = new Map<string, WeaponMeta>()
    const cls = "Weapon"

    if (json) {
      for (const [key, raw] of Object.entries(json)) {
        let value: number[] = raw as number[]

        checkIsArray(value, cls, "weaponValue")

        let weapon = new WeaponMeta(key,
          assignString(value[0], cls, "[0]/name"),
          assignString(value[1], cls, "[1]/type"),
          assignNumber(value[2], cls, "[2]/dmg1"),
          assignNumber(value[3], cls, "[3]/dmg2"),
          assignNumber(value[4], cls, "[4]/ss"),
          assignNumber(value[5], cls, "[5]/acc"),
          assignNumber(value[6], cls, "[6]/rof1"),
          assignNumber(value[7], cls, "[7]/rof2"),
          assignNumber(value[8], cls, "[8]/recoil"),
          assignNumber(value[9], cls, "[9]/range"),
          assignNumber(value[10], cls, "[10]/ammo"),
          assignNumber(value[11], cls, "[11]/ammo"),
        )

        weaponsMap.set(key, weapon)
      }
    } else {
      throw new Error(`Raw json for ${cls} is empty`)
    }

    return weaponsMap;
  }

  private static loadCounters(model: Gurps): Map<string, CounterMeta> {

    let counters = new Map<string, CounterMeta>()


    counters.set("Fatigue", new CounterMeta("Fatigue", "Fatigue", CounterKind.gauge,
      [model.attributes.get(PrimaryAttributeKeys.ST)!, model.secondaryAttributes.get("extrafatigue")!],
      (cv, cs) => {
        cv.base = cs.attributes.ST.base + cs.secondaryAttributes.get("extrafatigue")!.base
        cv.cached = cs.attributes.ST.cached + cs.secondaryAttributes.get("extrafatigue")!.cached
      }))

    counters.set("Hits", new CounterMeta("Hits", "Hit points", CounterKind.gauge,
      [model.attributes.get(PrimaryAttributeKeys.HT)!, model.secondaryAttributes.get("extrahp")!],
      (cv, cs) => {
        cv.base = cs.attributes.HT.base + cs.secondaryAttributes.get("extrahp")!.base
        cv.cached = cs.attributes.HT.cached + cs.secondaryAttributes.get("extrahp")!.cached
      }))

    counters.set("Speed", new CounterMeta("Speed", "Speed", CounterKind.passive,
      [model.attributes.get(PrimaryAttributeKeys.HT)!, model.attributes.get(PrimaryAttributeKeys.DX)!],
      (cv, cs) => {
        cv.base = (cs.attributes.HT.base + cs.attributes.DX.base) / 4
        cv.cached = (cs.attributes.HT.cached + cs.attributes.DX.cached) / 4
      }))

    counters.set("Move", new CounterMeta("Move", "Move", CounterKind.passive,
      [counters.get("Speed")!
      ],
      (cv, cs) => {
        cv.base = Math.floor(cs.counters.get("Speed")!.base)
        cv.cached = Math.floor(cs.counters.get("Speed")!.cached)
      }))

    counters.set("PD", new CounterMeta("PD", "Passive Defence (PD)", CounterKind.rollable,
      [],
      (cv, cs) => {
        cv.base = cs.armor.meta.PD
        cv.cached = cs.armor.meta.PD
      }))
    counters.set("DR", new CounterMeta("DR", "Damage Resistance (DR)", CounterKind.passive,
      [],
      (cv, cs) => {
        cv.base = cs.armor.meta.DR
        cv.cached = cs.armor.meta.DR
      }))

    counters.set("RR", new CounterMeta("RR", "Radiation Resistance (RR)", CounterKind.passive,
      [],
      (cv, cs) => {
        cv.base = cs.armor.meta.RR
        cv.cached = cs.armor.meta.RR
      }))

    counters.set("armorHP", new CounterMeta("armorHP", "Armor hit points", CounterKind.gauge,
      [],
      (cv, cs) => {
        cv.base = cs.armor.meta.HP
        cv.cached = cs.armor.meta.HP
      }))

    counters.set("Dodge", new CounterMeta("Dodge", "Dodge", CounterKind.rollable,
      [counters.get("Move")!, counters.get("PD")!],
      (cv, cs) => {
        cv.base = cs.counters.get("Move")!.base + cs.counters.get("PD")!.base
        cv.cached = cs.counters.get("Move")!.cached + cs.counters.get("PD")!.cached
      }))

    counters.set("Parry", new CounterMeta("Parry", "Parry", CounterKind.rollable,
      [],
      (cv, _) => {
        cv.base = 0
        cv.cached = 0
      }))

    counters.set("Block", new CounterMeta("Block", "Block", CounterKind.rollable,
      [],
      (cv, _) => {
        cv.base = 0
        cv.cached = 0
      }))

    counters.set("Perception", new CounterMeta("Perception", "Perception", CounterKind.rollable,
      [model.attributes.get(PrimaryAttributeKeys.IQ)!, model.secondaryAttributes.get("alertness")!],
      (cv, cs) => {
        cv.base = cs.attributes.IQ.base + cs.secondaryAttributes.get("alertness")!.base
        cv.cached = cs.attributes.IQ.cached + cs.secondaryAttributes.get("alertness")!.cached
      }))

    counters.set("Willpower", new CounterMeta("Willpower", "Willpower", CounterKind.rollable,
      [model.attributes.get(PrimaryAttributeKeys.IQ)!, model.secondaryAttributes.get("will")!],
      (cv, cs) => {
        cv.base = cs.attributes.IQ.base + cs.secondaryAttributes.get("will")!.base
        cv.cached = cs.attributes.IQ.cached + cs.secondaryAttributes.get("will")!.cached
      }))

    counters.set("Malf", new CounterMeta("Malf", "Armor Malfunction", CounterKind.passive,
      [],
      (cv, cs) => {
        cv.base = Math.round(cs.counters.get("armorHP")!.base / 5)
        cv.cached = Math.round(cs.counters.get("armorHP")!.cached / 5)
      }))

    //a base_melee_damages  = get_base_melee_damages(ST)
    //a char_counts['Thrust'] = d_notation(base_melee_damages[0], base_melee_damages[1])
    //a char_counts['Swing'] = d_notation(base_melee_damages[2], base_melee_damages[3])
    //a char_counts['Kick'] = char_counts['Thrust'];
    //a char_counts['PsiPower'] = char_counts['Willpower'] + PsionicTalent;
    //a char_counts['PsiEnergy'] = char_counts['Willpower'] * PsionicTalent;

    return counters
  }

  private static ranged_mod_table = [
    1 / (36 * 10),
    1 / (36 * 5),
    1 / (36 * 3),
    1 / (36 * 2),
    2 / (36 * 3),
    1 / (36),
    2 / (36 * 1.5),
    2 / (36),
    3 / (36),
    6 / (36),
    12 / (36),
    1.5 / 3,
    2 / 3,
    1,
    1.5,
    2,
    3,
    4.5,
    7,
    10,
    15,
    20,
    30,
    45,
    70,
    100,
    150,
    200,
    300,
    450,
    700,
    1000,
    1500,
    2000,
    3000,
    4500,
    7000,
    10000,
    10 * 1600,
    15 * 1600,
    20 * 1600,
    200 * 1600,
    2000 * 1600,
    20000 * 1600,
    200000 * 1600]

  public static calcShootMod(value: number, onlyNegative: boolean = false) {

    let mod = Gurps.adjustMod(Gurps.getModRaw(value));

    if (onlyNegative && mod > 0) {
      mod = 0;
    }

    return mod
  }

  private static getModRaw(value: number) {
    let res = Gurps.ranged_mod_table.findIndex(breakpoint => breakpoint > value) - 1
    return res
  }

  private static adjustMod(value: number) {
    if (value < 41) return 15 - value;
    const anomalies = [-31, -37, -43, -49];
    return anomalies[value - 41];
  }
}

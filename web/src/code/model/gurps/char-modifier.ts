import {PrimaryAttributeKeys} from "./gurps-model";
import {Gurps} from "./gurps";
import {signify} from "../../lib/text";

export enum ModKind {
  skill,
  attribute,
  secondaryAttribute,
  counter,
  other
}

export class CharModifier {
  public static readonly NOT_FIXED = "?"

  public static readonly SRC_CUSTOM = "custom"
  public static readonly SRC_ARMOR = "armor"
  public static readonly SRC_TEMP_ROUND = "round"

  public subjects = new Set<string>();
  public id: string = ""

  // TODO: subjects should be linked with value
  constructor(
    public kind: ModKind,
    subject: string,
    public name: string,
    public source: string,
    public value: number,
    public conditional: boolean = false,
    public enabled: boolean = true,
    public temp: boolean = false
  ) {

    this.subjects.add(subject);
  }

  public get displayValue(): string {
    return signify(this.value)
  }

  static getKind(subject: string, model: Gurps): ModKind {

    let kind: ModKind

    if (PrimaryAttributeKeys.baseAttributes.has(subject)) {
      kind = ModKind.attribute;
    } else if (model.skills.has(subject)) {
      kind = ModKind.skill
    } else if (model.secondaryAttributes.has(subject)) {
      kind = ModKind.secondaryAttribute
    } else if (model.counters.has(subject)) {
      kind = ModKind.counter
    } else {
      kind = ModKind.other
    }

    return kind
  }

  public clone(): CharModifier {

    let cloned = new CharModifier(
      this.kind,
      CharModifier.NOT_FIXED,
      this.name,
      this.source,
      this.value,
      this.conditional,
      this.enabled,
      this.temp
    )

    cloned.subjects = this.subjects

    return cloned
  }
}



import {assignString} from "../../lib/assigns";
import {CharModifier, ModKind} from "./char-modifier";
import {CounterValue, GurpsCharacter} from "./gurps-character";

export class PrimaryAttributeKeys {
  public static readonly ST = "ST"
  public static readonly DX = "DX"
  public static readonly IQ = "IQ"
  public static readonly HT = "HT"

  public static readonly baseAttributes = new Set<string>([PrimaryAttributeKeys.ST, PrimaryAttributeKeys.DX, PrimaryAttributeKeys.IQ, PrimaryAttributeKeys.HT]);

  public static parse(obj: any): string {
    let value = assignString(obj, "attribute", "name")

    if (PrimaryAttributeKeys.baseAttributes.has(value)) {
      return value
    } else {
      throw new Error("Unexpected Attribute name: " + value)
    }
  }
}

export class BaseMeta {
  constructor(
    public key: string,
    public name: string,
    public modKind: ModKind,
    public components: BaseMeta[] = []) {
  }
}

export class AttributeMeta extends BaseMeta {
  constructor(key: string, name: string) {
    super(key, name, ModKind.attribute)
  }
}

export class SecondaryAttributeMeta extends BaseMeta {
  constructor(key: string, name: string, public deprecated: boolean = false) {
    super(key, name, ModKind.secondaryAttribute)
  }
}

export enum CounterKind {
  gauge,
  rollable,
  passive
}


export class CounterMeta extends BaseMeta {

  constructor(key: string,
              name: string,
              public kind: CounterKind,
              components: BaseMeta[],
              public valueCalculator = (_: CounterValue, __: GurpsCharacter) => {
              }
  ) {
    super(key, name, ModKind.counter, components)
  }
}

export class SkillMeta extends BaseMeta {

  constructor(key: string,
              name: string,
              public hardness: string,
              public attribute: AttributeMeta,
              public baseValue: number) {

    super(key, name, ModKind.skill, [attribute])

    //recalculate base value
    this.recalcBaseValue()

  }

  private recalcBaseValue() {
    //Strange, should differ Phys/Mental, but doesn't

    let hardnessLevelCode = this.hardness.charAt(1)
    let hardnessMap: Map<string, number> = new Map([["E", -2], ["A", -3], ["H", -4], ["V", -5]])
    let hardnessValue = hardnessMap.get(hardnessLevelCode)
    if (!hardnessValue) {
      throw new Error(`Skill ${this.name} hardness ${this.hardness} can't be parsed`)
    }

    this.baseValue = hardnessValue
  }
}

export enum AdvantageKind {
  advantage,
  disadvantage
}

export class AdvantageMeta {

  public modifiers: CharModifier[] = []

  constructor(public key: string,
              public name: string,
              public kind: AdvantageKind,
              public cpPrice: number,
              public maxCount: number) {
  }

}

export class ArmorMeta {
  constructor(public key: string,
              public name: string,
              public PD: number,
              public DR: number,
              public RR: number,
              public HP: number,
              public modifiers: CharModifier[]) {
  }
}

export class WeaponMeta {
  constructor(public key: string,
              public name: string,
              public dmgType: string,
              public dmg1: number,
              public dmg2: number,
              public ss: number,
              public acc: number,
              public rof1: number,
              public rof2: number,
              public recoil: number,
              public range: number,
              public ammo: number,
              public minST: number
  ) {
  }
}

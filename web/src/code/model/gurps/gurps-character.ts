import {AdvantageMeta, ArmorMeta, AttributeMeta, BaseMeta, CounterMeta, PrimaryAttributeKeys, SkillMeta, WeaponMeta} from "./gurps-model";
import {CharModifier, ModKind} from "./char-modifier";


export class GurpsCharacter {

  public static readonly PROTOCOL_VERSION = "0.2"

  public name!: string
  public id!: string;
  public settingId: string = ""
  public settingName: string = ""

  public attributes = new PrimaryAttributeValues(new Map<string, AttributeValue>())
  public secondaryAttributes = new Map<string, SecondaryAttributeValue>()
  public advantages = new Map<string, AdvantageValue>()
  public counters = new Map<string, CounterValue>()
  public skills = new Map<string, SkillValue>()

  public modifiers: CharModifier[] = []
  public armor!: EquippedArmor;
  public weapons: EquippedWeapon[] = [];
  public activeWeapon: EquippedWeapon | null = null
  public protocolVersion: string = "";


  public getModsBySubject(subject: string, kind: ModKind): CharModifier[] {

    return this.modifiers
      .filter(m => m.kind === kind)
      .filter(m => m.subjects.has(subject))
  }

  public recalculateCharacter() {
    this.attributes.map.forEach(attribute => {
      attribute.recalculate(this)
    })
    this.secondaryAttributes.forEach(attribute => {
      attribute.recalculate(this)
    })
    this.skills.forEach(skill => {
      skill.recalculate(this)
    })

    this.counters.forEach(counter => {
      counter.recalculate(this)
    })
  }

}

export class ValuesSet<T> {

  constructor(public map: Map<string, T> = new Map<string, T>()) {
  }
}


export class PrimaryAttributeValues extends ValuesSet<AttributeValue> {


  constructor(map: Map<string, AttributeValue>) {
    super(map);
  }

  public get ST(): AttributeValue {
    return this.map.get(PrimaryAttributeKeys.ST)!
  }

  public get DX(): AttributeValue {
    return this.map.get(PrimaryAttributeKeys.DX)!
  }

  public get IQ(): AttributeValue {
    return this.map.get(PrimaryAttributeKeys.IQ)!
  }

  public get HT(): AttributeValue {
    return this.map.get(PrimaryAttributeKeys.HT)!
  }
}


export class ModifiableValue {

  public componentsValues: ModifiableValue[] = [];

  public cached: number = 0
  public appliedModifiers: CharModifier[] = []
  public modified: boolean = false;
  private calculated: boolean = false

  constructor(public baseMeta: BaseMeta,
              protected key: string,
              public base: number = 0) {
    this.cached = base
  }

  public get changedBy(): number {
    return this.cached - this.base
  }

  public applyModifiers(cs: GurpsCharacter): number {
    let mods = cs.getModsBySubject(this.key, this.baseMeta.modKind)
    this.appliedModifiers = []

    let change = 0;
    mods.forEach(mod => {
      if (mod.enabled && !mod.conditional) {
        change += mod.value
        this.appliedModifiers.push(mod)
      }
    })

    return change
  }

  public recalculate(cs: GurpsCharacter) {
    const mod = this.applyModifiers(cs)
    this.cached = this.base + mod

    this.initComponents(cs)
  }

  protected initComponents(cs: GurpsCharacter) {

    if (!this.calculated) {
      let components = new ComponentsSplit()
      this.findComponents(cs, this.baseMeta.components, components)
      this.componentsValues = components.flatten()

      this.modified = false
      this.calculateIsModified()
    }
  }

  private calculateIsModified() {
    let modified = this.appliedModifiers.length > 0
    if (!modified) {
      let modifiedComponents = this.componentsValues.filter(cv => cv.modified)
      modified = modifiedComponents.length > 0
    }

    this.modified = modified
  }


  private findComponents(cs: GurpsCharacter, components: BaseMeta[], componentsSplit: ComponentsSplit) {

    components.forEach(base => {

      switch (base.modKind) {
        case ModKind.attribute:
          componentsSplit.attributes.set(base.key, cs.attributes.map.get(base.key)!)
          break;
        case ModKind.secondaryAttribute:
          componentsSplit.secAttributes.set(base.key, cs.secondaryAttributes.get(base.key)!)
          break;
        case ModKind.counter:
          componentsSplit.counters.set(base.key, cs.counters.get(base.key)!)
          break;
        case ModKind.skill:
          componentsSplit.skills.set(base.key, cs.skills.get(base.key)!)
          break;
      }

      // gather child components
      this.findComponents(cs, base.components, componentsSplit)

    })
  }
}

class ComponentsSplit {
  attributes: Map<string, ModifiableValue> = new Map<string, ModifiableValue>()
  secAttributes: Map<string, ModifiableValue> = new Map<string, ModifiableValue>()
  counters: Map<string, ModifiableValue> = new Map<string, ModifiableValue>()
  skills: Map<string, ModifiableValue> = new Map<string, ModifiableValue>()

  public flatten(): ModifiableValue[] {
    let flats: ModifiableValue[] = []

    this.attributes.forEach(v => {
      flats.push(v)
    })

    this.secAttributes.forEach(v => {
      flats.push(v)
    })

    this.counters.forEach(v => {
      flats.push(v)
    })

    this.skills.forEach(v => {
      flats.push(v)
    })

    return flats
  }
}


export class AttributeValue extends ModifiableValue {

  constructor(public meta: AttributeMeta,
              base: number) {
    super(meta, meta.key, base)
  }
}


export class SecondaryAttributeValue extends ModifiableValue {

  constructor(public meta: AttributeMeta,
              base: number) {
    super(meta, meta.key, base)
  }
}


export class SkillValue extends ModifiableValue {

  constructor(public meta: SkillMeta,
              public increment: number,
              public spentCP: number,
              base: number) {
    super(meta, meta.key, base)
  }

  public override recalculate(cs: GurpsCharacter) {

    let baseAttribute = cs.attributes.map.get(this.meta.attribute.name)!
    this.base = baseAttribute.base + this.meta.baseValue + this.increment
    let mod = super.applyModifiers(cs)

    // apply attribute mods value
    this.cached = this.base + (baseAttribute.cached - baseAttribute.base) + mod
    this.initComponents(cs)
  }
}


export class CounterValue extends ModifiableValue {

  public baseComponents = new Map<string, ModifiableValue>()

  constructor(public meta: CounterMeta) {

    super(meta, meta.key, 0)
  }

  public override recalculate(cs: GurpsCharacter) {

    this.meta.valueCalculator(this, cs)

    let mod = super.applyModifiers(cs)
    this.cached = this.cached + mod

    this.initComponents(cs)
  }
}

export class AdvantageValue {

  constructor(public meta: AdvantageMeta,
              public taken: number) {

  }
}

export class EquippedArmor {

  constructor(public meta: ArmorMeta) {
  }
}

export class EquippedWeapon {

  constructor(public meta: WeaponMeta,
              public quantity: number) {
  }
}

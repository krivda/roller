export enum RollType {
  checkRoll = "checkRoll",
  damageRoll = "damageRoll"
}

export abstract class Roll {

  protected constructor(public rollType: string,
                        public character: string,
                        public description: string,
                        public secret: boolean) {
  }

  protected rollDices(count: number, side: number, isCritical: boolean = false): number[] {

    let res: number[] = []
    for (let i = 0; i < count; i++) {

      if (isCritical) {
        res.push(side)
      } else {
        res.push(this.rollDice(side))
      }
    }

    return res;
  }

  protected rollDice(side: number): number {
    return 1 + Math.floor(Math.random() * side);
  }
}

export class RollResult {
  constructor(public dices: number[],
              public sum: number) {
  }
}

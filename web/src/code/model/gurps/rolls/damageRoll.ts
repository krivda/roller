import {Roll, RollResult, RollType} from "../rolls";

export class DamageRollResult extends RollResult {
  constructor(dices: number[],
              sum: number) {
    super(dices, sum);
  }
}

export class DamageRoll extends Roll {
  public result: DamageRollResult = new DamageRollResult([], 0);

  constructor(character: string,
              description: string,
              public diceCount: number,
              public addendum: number,
              public multiplier: number,
              public DR: number,
              public isCritical: boolean,
              secret: boolean
  ) {
    super(RollType.damageRoll, character, description, secret)
  }

  public roll(): DamageRollResult {

    let rd = this.rollDices(this.diceCount, 6, this.isCritical)

    let sum = 0
    rd.forEach(n => {
      sum += n
    })

    sum = sum + this.addendum
    sum = sum - this.DR
    if (sum < 0) sum = 0

    const damage = sum * this.multiplier

    this.result = new DamageRollResult(rd, damage)
    return this.result
  }
}

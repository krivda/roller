import {Roll, RollResult, RollType} from "../rolls";

export enum OutcomeKind {
  astonishingSuccess,
  criticalSuccess,
  success,
  relativeSuccess,
  failure,
  criticalFailure,
  catastrophicFailure
}

export class CheckRollResult extends RollResult {
  constructor(public outcome: boolean,
              public outcomeKind: OutcomeKind,
              public outcomeText: string,
              dices: number[],
              result: number) {
    super(dices, result);
  }
}

export class CheckRoll extends Roll {

  public result!: CheckRollResult
  public totalDifficulty: number = 0

  constructor(character: string,
              description: string,
              public subject: string,
              public subjectValue: number,
              public difficultyAdjust: number,
              secret: boolean
  ) {
    super(RollType.checkRoll, character, description, secret)
    this.totalDifficulty = subjectValue + difficultyAdjust

    if (this.totalDifficulty < 1) this.totalDifficulty = 1
    if (this.totalDifficulty > 18) this.totalDifficulty = 18
  }

  public roll(): RollResult {
    let rd = this.rollDices(3, 6)

    let sum = 0
    rd.forEach(n => {
      sum += n
    })


    let outcome: boolean
    let outcomeKind: OutcomeKind
    let outcomeText: string

    if (rd.filter(d => d === 1).length === rd.length) {
      outcome = true
      outcomeKind = OutcomeKind.astonishingSuccess
      outcomeText = "ОШЕЛОМИТЕЛЬНЫЙ УСПЕХ"
    } else if (this.totalDifficulty - sum > 8) {
      outcome = true
      outcomeKind = OutcomeKind.criticalSuccess
      outcomeText = "КРИТИЧЕСКИЙ УСПЕХ"
    } else if (sum < this.totalDifficulty) {
      outcome = true
      outcomeKind = OutcomeKind.success
      outcomeText = "УСПЕХ"
    } else if (sum === this.totalDifficulty) {
      outcome = true
      outcomeKind = OutcomeKind.relativeSuccess
      outcomeText = "ОТНОСИТЕЛЬНЫЙ УСПЕХ"
    } else if (rd.filter(d => d === 6).length === rd.length) {
      outcome = false
      outcomeKind = OutcomeKind.catastrophicFailure
      outcomeText = "КАТАСТРОФИЧЕСКИЙ ПРОВАЛ"
    } else if (sum - this.totalDifficulty > 8) {
      outcome = false
      outcomeKind = OutcomeKind.criticalFailure
      outcomeText = "КРИТИЧЕСКИЙ ПРОВАЛ"
    } else {
      outcome = false
      outcomeKind = OutcomeKind.failure
      outcomeText = "ПРОВАЛ"
    }

    this.result = new CheckRollResult(outcome, outcomeKind, outcomeText, rd, sum)
    return this.result
  }
}

import * as Jsep from "jsep";
import {BinaryExpression, CallExpression, Expression, Identifier, Literal} from "jsep";

enum ExprTypes {
  BinaryExpression = "BinaryExpression",
  Literal = "Literal",
  CallExpression = "CallExpression",
  IdentifierExpression = "Identifier",
}

export interface ExpressionIdentifierResolver {
  resolve(name: string): number
}

export class MappedIdentifierResolver implements ExpressionIdentifierResolver {

  private idMap: Map<string, number> = new Map<string, number>()

  resolve(name: string): number {
    let res = this.idMap.get(name);

    if (res == null) {
      throw new Error(`Identifier '${name}' is not defined`)
    }

    return res
  }
}

export interface ExpressionFunctionResolver {
  resolve(name: string, parameters: any[]): number
}

export class MappedFunctionResolver implements ExpressionFunctionResolver {

  private idMap: Map<string, number> = new Map<string, number>()

  resolve(name: string, parameters: any[]): number {
    let res = this.idMap.get(name);

    if (res == null) {
      throw new Error(`Function '${name}' is not defined`)
    }

    return res
  }
}

export class ExpressionEvaluator {

  public evaluate(expression: string, identifiers: ExpressionIdentifierResolver, functions: ExpressionFunctionResolver): number {
    let expTree = this.blackMagic(expression)
    let res = this.evalAny(expTree, identifiers, functions)
    return res
  }

  public evalAny(exp: Expression, identifiers: ExpressionIdentifierResolver, functions: ExpressionFunctionResolver): number {

    let res

    switch (exp.type) {
      case ExprTypes.Literal:
        res = this.evalLiteral(exp as Literal)
        break;
      case ExprTypes.BinaryExpression:
        res = this.evalBinary(exp as BinaryExpression, identifiers, functions)
        break;
      case ExprTypes.CallExpression:
        res = this.evalFunction(exp as CallExpression, identifiers, functions)
        break;
      case ExprTypes.IdentifierExpression:
        res = this.evalIdentifier(exp as Identifier, identifiers)
        break;
      default:
        throw new Error(`Expression type '${exp.type}' is not allowed`)
    }

    return res
  }

  private evalLiteral(exp: Literal): number {
    if (typeof exp.value !== "number") {
      throw new Error(`Expression type '${exp.type}' is not allowed`)
    }

    return exp.value as number
  }

  private evalBinary(exp: BinaryExpression, identifiers: ExpressionIdentifierResolver, functions: ExpressionFunctionResolver): number {

    let left = this.evalAny(exp.left, identifiers, functions)
    let right = this.evalAny(exp.right, identifiers, functions)

    let res: number
    switch (exp.operator) {
      case "+":
        res = left + right
        break;
      case "-":
        res = left - right
        break;
      case "*":
        res = left * right
        break;
      case "/":
        res = left / right
        break;
      default:
        throw new Error(`Operator '${exp.operator} is not supported'`)
    }

    return res
  }

  private evalFunction(exp: CallExpression, identifiers: ExpressionIdentifierResolver, functions: ExpressionFunctionResolver): number {

    let name
    let res: number
    if (exp.callee.type !== "Identifier") {
      throw new Error(`Callee type '${exp.callee.type} is not supported'`)
    } else {
      name = (exp.callee as unknown as Identifier).name
    }

    let params: any[] = []
    exp.arguments.forEach(p => {

      let param
      //naive support for not-int params
      if (p.type === ExprTypes.Literal) {
        param = (p as Literal).value
      } else {
        param = this.evalAny(p, identifiers, functions)
      }

      params.push(param)
    })

    res = functions.resolve(name, params)

    return res
  }

  private evalIdentifier(exp: Identifier, identifiers: ExpressionIdentifierResolver): number {
    return identifiers.resolve(exp.name)
  }

  private blackMagic(expression: string): Expression {

    let jsepModule: any
    Object.values(Jsep)
      .filter(fn => typeof fn === "function")
      .map(fn => jsepModule = fn);


    let exprTree: Expression = jsepModule(expression)
    return exprTree
  }
}



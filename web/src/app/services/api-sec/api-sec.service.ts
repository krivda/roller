import {Injectable} from "@angular/core";
import {CryptoService} from "../crypto/crypto.service";

@Injectable({
  providedIn: "root"
})
export class ApiSecService {

  public readonly FIREBASE_KEY = "firebase"
  public readonly KAFKA_KEY = "kafka"
  public secMap: Map<string, any> = new Map<string, string>()

  constructor(private cryptoService: CryptoService) {
  }

  serializeSecrets(secret: string, entries: Map<string, string>): Map<string, string> {
    return this.cryptoService.encryptMap(entries, secret)
  }

  unpackSecrets(apiSec: Map<string, any>, secret: string) {
    this.secMap = this.cryptoService.decryptMap(apiSec, secret, true)
  }
}

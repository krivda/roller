import {TestBed} from "@angular/core/testing";

import {ApiSecService} from "./api-sec.service";

describe("ApiSecService", () => {
  let service: ApiSecService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiSecService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});

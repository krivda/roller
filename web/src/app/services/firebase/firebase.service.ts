import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {FirebaseApp, FirebaseOptions} from "@firebase/app";
import * as firebase from "firebase/app";
import {Database, off} from "firebase/database";
import {DatabaseReference, getDatabase, onValue, ref} from "@firebase/database";
import {ApiSecService} from "../api-sec/api-sec.service";
import {CharacterLoaderService} from "../character/character-loader.service";
import {Pair} from "../../../code/lib/pair";

@Injectable({
  providedIn: "root"
})
export class FirebaseService {
  private app: FirebaseApp | null = null;
  private database: Database | null = null;

  private refs: DatabaseReference[] = []

  constructor(private apiSec: ApiSecService) {
  }

  public connectToDatabase() {

    let firebaseOptions: FirebaseOptions = this.apiSec.secMap.get(this.apiSec.FIREBASE_KEY) as FirebaseOptions

    if (!firebaseOptions) {
      throw Error("Firebase secrets not loaded!")
    }
    try {
      if (!this.app) {
        this.app = firebase.initializeApp(firebaseOptions)
      }
    } catch (err) {
      console.log(err)
      throw err
    }

    this.database = getDatabase(this.app)
  }

  public linkToCharacter(db: string, characterId: string): Observable<any> {

    if (!this.database) {
      throw new Error("Database not linked. please call linkToDatabase() prior to querying")
    }

    let query
    if (characterId === CharacterLoaderService.ALL) {
      query = db
    } else {
      query = `${db}/${characterId}`
    }


    const charLoader = ref(this.database, query)
    this.refs.push(charLoader)
    const watcher = new Subject<Pair<string, any>>()

    onValue(charLoader, snapshot => {

      if (snapshot.key === characterId) {
        let val = snapshot.val()
        if (val) {
          watcher.next({key: snapshot.key, value: snapshot.val()} as Pair<string, any>)
        }
      } else {
        snapshot.forEach(child => {
          watcher.next({key: child.key, value: child.val()} as Pair<string, any>)
        })
      }
    })

    return watcher
  }

  public logoff() {
    if (this.database) {
      this.refs.forEach(r => off(r))
      console.log("disconnected from firebase")
    }
  }


  // AngularFireModule.initializeApp(response.config);
  // initializeFirebase() {
  //   const url = 'http://localhost:3000/get-config?db=12345';
  //   this.httpClient.get<any>(url).subscribe(response => {
  //
  //   });
  // }
}

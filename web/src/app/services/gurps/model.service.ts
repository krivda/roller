import {Injectable} from "@angular/core";
import {Gurps} from "../../../code/model/gurps/gurps";
import {advDB, armorDB, skillsDB, weaponDB} from "../../../code/static-db/static";

//hard load form embedded internal db
const orion_db: any = {
  skills: skillsDB,
  advantages: advDB,
  weapons: weaponDB,
  armors: armorDB
}

@Injectable({
  providedIn: "root"
})
export class ModelService {

  gurps: Gurps

  constructor() {
    // noinspection UnnecessaryLocalVariableJS
    let json = orion_db // nowhere to get db. getting from statics
    this.gurps = Gurps.loadModel(json)
  }

  public loadGurps(): Gurps {
    return this.gurps
  }
}

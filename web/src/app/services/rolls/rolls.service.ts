import {Injectable} from "@angular/core";
import {GurpsCharacter, ModifiableValue} from "../../../code/model/gurps/gurps-character";
import {Roll} from "../../../code/model/gurps/rolls";
import {Observable, Subject} from "rxjs";
import {ExpressionEvaluator, MappedFunctionResolver, MappedIdentifierResolver} from "../../../code/model/ast/ast-eval";
import {CheckRoll} from "../../../code/model/gurps/rolls/checkRoll";
import {DamageRoll} from "../../../code/model/gurps/rolls/damageRoll";
import {WeaponMeta} from "../../../code/model/gurps/gurps-model";

@Injectable({
  providedIn: "root"
})
export class RollsService {

  private expressionEvaluator = new ExpressionEvaluator()

  private rollNotifier$ = new Subject<Roll>()

  constructor() {
  }

  public makeCheckRoll(rollSubject: ModifiableValue, character: GurpsCharacter,
                       difficulty: number, description: string, secret: boolean): CheckRoll {

    let roll = new CheckRoll(
      character.name,
      description,
      rollSubject.baseMeta.name,
      rollSubject.cached,
      difficulty, secret)

    roll.roll()
    this.publishRoll(roll)

    return roll
  }

  public makeDamageRoll(weapon: WeaponMeta, character: GurpsCharacter, DR: number, isCriticalDamage: boolean, secret: boolean): DamageRoll {

    let roll = new DamageRoll(
      character.name, weapon.name, weapon.dmg1, weapon.dmg2,
      weapon.rof2, DR, isCriticalDamage, secret)


    roll.roll()
    this.publishRoll(roll)

    return roll
  }

  public evalExpr(expression: string): number {
    let res = this.expressionEvaluator.evaluate(expression, new MappedIdentifierResolver(), new MappedFunctionResolver())
    return res
  }

  public getRollsUpdater(): Observable<Roll> {
    return this.rollNotifier$
  }

  private publishRoll(roll: Roll) {
    this.rollNotifier$.next(roll)
  }


}

import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Pair} from "../../../code/lib/pair";
import {pairsArrayToMap} from "../../../code/lib/assigns";


@Injectable({
  providedIn: "root"
})
export class DocLoaderService {

  //local: ./assets/mydata.json

  constructor(private http: HttpClient) {
  }

  public loadJsonAsync(url: string): Observable<any> {
    return this.http.get<DocLoaderService>(url);
  }

  public loadJsonAsset(name: string): Observable<any> {
    return this.loadJsonAsync(`assets/${name}.json`)
  }

  public mapToJson(map: Map<string, string>): string {
    let records: Pair<string, string>[] = []

    map.forEach((v, k) => {
      records.push({key: k, value: v})
    })

    return JSON.stringify(records, null, 2)
  }

  public jsonToMap(json: string): Map<string, string> {
    let records = JSON.parse(json) as Pair<string, string>[]
    return pairsArrayToMap(records)
  }

  public loadMappedAsset(name: string): Observable<Map<string, string>> {
    return this.loadJsonAsset(name).pipe(
      map(json => pairsArrayToMap(json)),
    )
  }

}


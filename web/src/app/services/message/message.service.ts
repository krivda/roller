import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MessageService {

  private messageSource$: Subject<string> = new Subject<string>()

  constructor() {
  }

  public publishMessage(message: string) {
    this.messageSource$.next(message)
  }

  public getMessagesUpdater(): Observable<string> {
    return this.messageSource$
  }
}

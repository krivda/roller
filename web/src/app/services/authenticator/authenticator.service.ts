import {Injectable} from "@angular/core";
import {CryptoService} from "../crypto/crypto.service";
import {BehaviorSubject, map, of, switchMap, tap} from "rxjs";
import {DocLoaderService} from "../doc-loader/doc-loader.service";
import {AssignsMapping, assignString, checkIsArray, mapMeta, mapObjects, pairsArrayToMap} from "../../../code/lib/assigns";
import {Pair} from "../../../code/lib/pair";
import {ApiSecService} from "../api-sec/api-sec.service";

export interface Database {
  name: string;
  id: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthenticatorService {

  public static API_ROLE: string = "apiUser";
  public static ADMIN_ROLE = "adminUser"

  private static CURRENT_USER: string = "currentUser"

  public isAuthenticated$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
  public failedMessage$: BehaviorSubject<string> = new BehaviorSubject<string>("")
  public loggedUser: AppUser | null = null;


  constructor(
    private docLoader: DocLoaderService,
    private cryptoService: CryptoService,
    private apiSecService: ApiSecService) {

    //check local user
    let localUser = this.loadUserSession()
    if (localUser) {
      of(localUser).pipe(
        switchMap(user => this.docLoader.loadJsonAsset(`campaigns/${user.campaignId}`).pipe(
          map((json) => this.authenticateInCampaign(user, user.pwd, json, user.campaignId))))
      ).subscribe({
        next: (user) => {
          this.setCurrentUser(user, false)
        },
        error: (error) => {
          console.error(error)
          this.logoff()
          if (error instanceof Error)
            this.failedMessage$.next(error.message)
        }
      })
    }
  }

  public authenticateByPassword(login: string, password: string, campaignId: string) {

    this.logoff()
    this.docLoader.loadJsonAsset("auth").pipe(
      map(json => this.findUserByLogin(json, login)),
      tap(user => this.validatePassword(user, password)),
      tap(user => {
        user.campaignId = campaignId
        user.pwd = password
      }),
      switchMap(user => this.docLoader.loadJsonAsset(`campaigns/${campaignId}`).pipe(
        map((json) => this.authenticateInCampaign(user, password, json, campaignId))))
    ).subscribe({
      next: (user) => {
        this.setCurrentUser(user, false)
      },
      error: (error) => {
        console.error(error)
        this.logoff()
        if (error instanceof Error)
          this.failedMessage$.next(error.message)
      }
    })
  }
  public logoff() {
    sessionStorage.removeItem(AuthenticatorService.CURRENT_USER)
    this.failedMessage$.next("")
    this.isAuthenticated$.next(false)
    this.loggedUser = null
  }

  private loadUserSession(): AppUser | undefined {

    let user = undefined
    let preAuthJson = sessionStorage.getItem(AuthenticatorService.CURRENT_USER)
    if (preAuthJson) {
      user = JSON.parse(preAuthJson) as AppUser
    }

    return user
  }

  private setCurrentUser(user: AppUser, isLocalUser: boolean) {

    if (!isLocalUser) {
      let localJson = JSON.stringify(user)
      sessionStorage.setItem(AuthenticatorService.CURRENT_USER, localJson)
      console.log("Authenticated user by password")
    } else {
      console.log("Authenticated local user")
    }

    this.loggedUser = user
    this.isAuthenticated$.next(true)
  }

  public serializeUserInfo(name: string, login: string, password: string): string {

    let user = {
      name: name,
      login: login,
      pwdHash: this.cryptoService.SHAHash(password)
    }

    let res = JSON.stringify(user, null, 2)
    return res
  }

  private findUserByLogin(json: any, login: string): AppUser {
    let users = json as AppUser[]
    users = users.filter(u => u.login === login)

    if (users.length === 0) {
      throw new Error(`user ${login} not found`)
    }

    if (users.length > 1) {
      throw new Error(`user ${login} not unique. Please consult your admin`)
    }

    let user = users[0]
    return user;
  }

  private validatePassword(user: AppUser, password: string) {
    let hash = this.cryptoService.SHAHash(password)

    if (hash !== user.pwdHash) {
      throw new Error("wrong password")
    }
  }

  private authenticateInCampaign(user: AppUser, password: string, campaignJson: any, campaignId: string): AppUser {

    let cls = `Campaign[${campaignId}]`
    const campaignName = assignString(campaignJson.campaignName, cls, "campaignName")
    cls = `Campaign[${campaignName}]`

    const campaign = new Campaign()
    let metaC = mapMeta(campaign)
    metaC.push(new AssignsMapping("player",
      (_, src, cls, prop): Player => {
        const arr = checkIsArray(src.players, cls, prop)
        const player = arr.find(p => p.login === user.login)

        if (player == undefined) {
          throw new Error(`User '${user.login}' is not authorized to access campaign ${campaignName}`)
        }

        return player
      }, false, true))

    metaC.find(p => p.prop == "apiSec")!.converter = (apiSec: Pair<string, string>[], _, cls, prop): Map<string, string> => {
      checkIsArray(apiSec, cls, prop)
      return pairsArrayToMap(apiSec)
    }

    mapObjects(campaignJson, campaign, metaC, cls)

    const player = new Player()
    mapObjects(campaign.player!, player, mapMeta(player), "player")
    campaign.player = player

    // decrypt dm with user pwd
    player.apiKey = this.cryptoService.decryptMessage(player.apiKey, password)

    if (player.dmKey !== "") {
      player.dmKey = this.cryptoService.decryptMessage(player.dmKey, password)
    }

    if (player.personalKey !== "") {
      player.personalKey = this.cryptoService.decryptMessage(player.personalKey, password)
    }

    // decrypt api. don't store it, to make things more obscure
    this.apiSecService.unpackSecrets(campaign.apiSec, player.apiKey)
    user.campaign = campaign

    return user
  }
}

export interface AppUser {
  login: string
  name: string
  pwd: string;
  pwdHash: string
  campaignId: string;
  campaign: Campaign;
}

export class Player {
  login: string = ""
  apiKey: string = ""
  personalKey: string = ""
  dmKey: string = ""
  characters: string[] = []

  public get isDM(): boolean {
    return this.dmKey !== ""
  }
}

export class Campaign {
  campaignId: string = ""
  campaignName: string = ""
  database: string = ""
  dmKeyCheck: string = "" // защифрованная на DmKey campaignId
  apiSec: Map<string, any> = new Map<string, any>()
  player?: Player
}

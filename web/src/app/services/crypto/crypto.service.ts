import {Injectable} from "@angular/core";
import {AES, enc, SHA256} from "crypto-js";

const DECRYPTED: string = "decrypted"

@Injectable({
  providedIn: "root"
})
export class CryptoService {

  public AESEncryptString(value: string, key: string): string {
    let res = AES.encrypt(value, key).toString();
    return res
  }

  public AESDecryptString(value: string, key: string): string {
    let res = AES.decrypt(value, key).toString(enc.Utf8);
    return res
  }

  public SHAHash(value: string): string {
    let res = SHA256(value).toString();
    return res
  }

  public encryptMessage(message: string, key: string): string {
    let msg: CryptoMessage = {
      decrypted: DECRYPTED, value: message
    }
    let json = JSON.stringify(msg)

    return this.AESEncryptString(json, key)
  }

  public decryptMessage(crypto: string, key: string): string {
    let decrypt = this.AESDecryptString(crypto, key)

    let message
    try {
      message = JSON.parse(decrypt) as CryptoMessage
    } catch (err) {
      if (err instanceof SyntaxError) {
        throw new Error("Decryption failed. Result is not JSON")
      } else throw err
    }

    if (message.decrypted !== DECRYPTED) {
      throw new Error("Decryption failed")
    }

    return message.value
  }

  public encryptMap(map: Map<string, string>, key: string): Map<string, string> {
    const resMap = new Map<string, string>()

    map.forEach((v, k) => {
      let encrypted = this.encryptMessage(v, key)
      resMap.set(k, encrypted)
    })

    return resMap
  }

  public decryptMap<V>(cryptoMap: Map<string, string>, key: string, isJson: boolean): Map<string, V> {
    const resMap = new Map<string, V>()

    cryptoMap.forEach((v, k) => {
      let decrypted = this.decryptMessage(v, key)
      if (isJson) {
        const jv = JSON.parse(decrypted) as V
        resMap.set(k, jv)
      } else {
        resMap.set(k, decrypted as any)
      }
    })

    return resMap
  }
}

export interface CryptoMessage {
  decrypted: string, //always === DECRYPTED. Just a marker, that decryption worked
  value: string
}

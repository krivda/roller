import {Injectable} from "@angular/core";
import {Gurps} from "../../../code/model/gurps/gurps";
import {PrimaryAttributeKeys} from "../../../code/model/gurps/gurps-model";
import {assignNumber, assignString} from "../../../code/lib/assigns";
import {
  AdvantageValue,
  AttributeValue,
  CounterValue,
  EquippedArmor,
  EquippedWeapon,
  GurpsCharacter,
  PrimaryAttributeValues,
  SecondaryAttributeValue,
  SkillValue
} from "../../../code/model/gurps/gurps-character";
import {ModelService} from "../gurps/model.service";
import {DocLoaderService} from "../doc-loader/doc-loader.service";
import {filter, map, mergeAll, mergeMap, Observable, of} from "rxjs";
import {CharModifier, ModKind} from "../../../code/model/gurps/char-modifier";
import {FirebaseService} from "../firebase/firebase.service";
import {AppUser} from "../authenticator/authenticator.service";
import {Pair} from "src/code/lib/pair";


@Injectable({
  providedIn: "root"
})
export class CharacterLoaderService {

  public static ALL: string = "*";

  constructor(private modelService: ModelService,
              private docLoaderService: DocLoaderService,
              private firebaseService: FirebaseService,
  ) {
  }

  public loadAll(user: AppUser): Observable<GurpsCharacter> {

    return of(user.campaign.database).pipe(
      map(db => {
        let characters = user.campaign.player?.characters ?? []
        if (db === "") {
          console.info("loading local characters DB")
        } else {
          console.info(`loading characters form Firebase DB ${db}`)
          this.firebaseService.connectToDatabase()
          if (user.campaign.player?.isDM) {
            // DM loads all remote characters
            characters = [CharacterLoaderService.ALL]
          }
        }

        return characters
      }),
      mergeAll(),
      mergeMap(characterId => {
        if (user.campaign.database === "") {
          return this.docLoaderService.loadJsonAsset(characterId).pipe(
            map(json => {
                return {key: characterId, value: json} as Pair<string, string>
              }
            ))
        } else {
          return this.firebaseService.linkToCharacter(user.campaign.database, characterId)
        }
      }),
      map(pair => {

        let char: GurpsCharacter | null = null
        try {
          char = this.loadFromOrion(pair.key, pair.value, this.modelService.loadGurps())
          console.info(`${char.name} (id=${char.id}) is loaded`)
        } catch (err) {
          if (err instanceof Error)
            console.error(err.message)
          else
            console.error(err)
        }
        return char
      }),
      filter(char => char != null),
      map(char => char as GurpsCharacter)
    )
  }

  private loadFromOrion(id: string, json: any, model: Gurps): GurpsCharacter {

    let cs = new GurpsCharacter()
    cs.id = id
    let cls = "OrionSheet"

    try {
      cs.settingId = assignString(json.setting, cls, "setting")

      //hardcoded setting
      if (cs.settingId === "orion_shehab_1") {
        cs.settingName = "Орион"
      }

      cs.name = assignString(json.char_stats.Charname, cls, "Charname")

      cs.protocolVersion = assignString(json.protocol_version, cls, "protocol_version")
      if (cs.protocolVersion !== GurpsCharacter.PROTOCOL_VERSION) {
        throw Error(`Unsupported protocol version '${cs.protocolVersion}', only ${GurpsCharacter.PROTOCOL_VERSION} is ok. Character ${cs.name} won't be loaded.`)
      }

      let map = new Map<string, AttributeValue>()
      map.set(PrimaryAttributeKeys.ST, new AttributeValue(model.attributes.get(PrimaryAttributeKeys.ST)!, assignNumber(json.char_stats.ST, cls, "ST")))
      map.set(PrimaryAttributeKeys.DX, new AttributeValue(model.attributes.get(PrimaryAttributeKeys.DX)!, assignNumber(json.char_stats.DX, cls, "DX")))
      map.set(PrimaryAttributeKeys.IQ, new AttributeValue(model.attributes.get(PrimaryAttributeKeys.IQ)!, assignNumber(json.char_stats.IQ, cls, "IQ")))
      map.set(PrimaryAttributeKeys.HT, new AttributeValue(model.attributes.get(PrimaryAttributeKeys.HT)!, assignNumber(json.char_stats.HT, cls, "HT")))
      cs.attributes = new PrimaryAttributeValues(map)

      this.loadSecondaryStats(json, model, cs);

      this.loadSkills(json, model, cs);
      this.loadAdvantages(json, model, cs);
      this.initCounters(model, cs)

      this.loadArmor(json, model, cs)
      this.loadWeapons(json, model, cs)

      this.loadModifiers(json, model, cs)


      cs.recalculateCharacter()
    } catch (err) {
      console.error(`character ${id} not loaded!:`)
      throw err
    }
    return cs
  }

  private loadSkills(json: any, model: Gurps, cs: GurpsCharacter) {
    let orionSkills = json.char_skills
    if (orionSkills) {

      for (const [key, raw] of Object.entries(orionSkills)) {
        let value: number[] = raw as number[]

        let cls = "SkillValue"
        let skillMeta = model.skills.get(key)
        if (!skillMeta) {
          throw new Error(`Skill '${key}' is not found in db`)
        }

        let skill = new SkillValue(skillMeta,
          assignNumber(value[0], cls, "[0/increment]"),
          assignNumber(value[1], cls, "[1/spentCP]"),
          assignNumber(value[2], cls, "[2/cache]"),
        )

        cs.skills.set(key, skill)

      }
    }
  }

  private loadSecondaryStats(json: any, model: Gurps, cs: GurpsCharacter) {

    let mapSecondaries = new Map<string, any>()

    for (const [key, raw] of Object.entries(json.char_stats)) {

      let val
      if (typeof (raw) === "string") {
        val = assignString(raw, "char_stats", key)
      } else {
        val = assignNumber(raw, "char_stats", key)
      }


      mapSecondaries.set(key, val)
    }

    model.secondaryAttributes.forEach((meta, key) => {

      if (!meta.deprecated) {
        let value = mapSecondaries.get(key)
        if (!value && value !== 0) {
          throw new Error(`Secondary attribute '${key}' is set in character`)
        }
        cs.secondaryAttributes.set(key, new SecondaryAttributeValue(model.secondaryAttributes.get(key)!, value))
      } else {
        console.warn(`${cs.id} contains deprecated attribute '${key}'`)
      }
    })
  }

  private loadModifiers(json: any, model: Gurps, cs: GurpsCharacter) {
    let modifiers = json.custom_mods
    if (modifiers) {

      //sample
      // "cm61": [
      //   -1,
      //   "DX",
      //   "custom",
      //   "Желтая пилюля потсэффект",
      //   0
      // ],

      for (const [key, raw] of Object.entries(modifiers)) {
        let values: any[] = raw as any[]
        let cls = "ModifierArray"

        let value = assignNumber(values[0], cls, "[0/value]")
        let subject = assignString(values[1], cls, "[1/subject]")
        let source = assignString(values[2], cls, "[2/source]")
        let name = assignString(values[3], cls, "[3/name]")
        let enabled = assignNumber(values[4], cls, "[4/enabled]") == 1

        let kind = CharModifier.getKind(subject, model)

        if (kind == ModKind.other) {
          throw Error(`Modifier ${source}/${name} for ${subject} cannot be bound to either skill of attribute!`)
        }

        let mod = new CharModifier(kind, subject, name, source, value, false, enabled, true)
        mod.id = key
        cs.modifiers.push(mod)

      }
    }
  }

  private loadArmor(json: any, model: Gurps, cs: GurpsCharacter) {

    let armorKey = assignString(json.armorid, "OrionChar", "armorid");

    let armor = model.armors.get(armorKey)
    if (!armor) {
      throw Error(`Orion char must wear an armor. But '${armorKey}' json.armorid is not an known armor!`)
    }

    cs.armor = new EquippedArmor(armor)

    armor.modifiers.forEach(mod => {
      cs.modifiers.push(mod.clone())
    })
  }

  private loadWeapons(json: any, model: Gurps, cs: GurpsCharacter) {

    let jsonWeapons = json.char_weapons
    if (jsonWeapons) {

      //sample
      // "cm61": [
      //   -1,
      //   "DX",
      //   "custom",
      //   "Желтая пилюля потсэффект",
      //   0
      // ],

      for (const [key, raw] of Object.entries(jsonWeapons)) {
        let values: any[] = raw as any[]
        let cls = "char_weapons[i]"

        let weaponMeta = model.weapons.get(key)
        if (!weaponMeta) {
          throw Error(` Weapon '${key}' is not found!`)
        }

        let eqWeapon = new EquippedWeapon(weaponMeta,
          assignNumber(values[0], cls, "[0]/quantity"))

        cs.weapons.push(eqWeapon)
      }
    }
  }

  private initCounters(model: Gurps, cs: GurpsCharacter) {

    model.counters.forEach(meta => {
      cs.counters.set(meta.key, new CounterValue(meta))
    })


    //a cs.counters["Fatigue"] = cs.attributes[PrimaryAttributes.ST]
    //a char_counts['Fatigue'] = ST + char_stats["extrafatigue"];
    //a char_counts['Hits'] = HT + char_stats["extrahp"];
    //a char_counts['Speed'] = (HT + DX)/4;
    //a char_counts['Move'] = Math.floor(char_counts['Speed']);
    //a char_counts['PD'] = 0;
    //a char_counts['DR'] = 0;
    //a char_counts['RR'] = 0;
    //a char_counts['armorHP'] = 0;
    //a char_counts['Malf'] = 0;
    //a char_counts['Dodge'] = char_counts['Move'] + char_counts['PD'];
    //a char_counts['Perception'] = IQ + char_stats["alertness"];
    //a char_counts['Willpower'] = IQ + char_stats["will"];
    //a base_melee_damages  = get_base_melee_damages(ST)
    //a char_counts['Thrust'] = d_notation(base_melee_damages[0], base_melee_damages[1])
    //a char_counts['Swing'] = d_notation(base_melee_damages[2], base_melee_damages[3])
    //a char_counts['Kick'] = char_counts['Thrust'];
    //a char_counts['PsiPower'] = char_counts['Willpower'] + PsionicTalent;
    //a char_counts['PsiEnergy'] = char_counts['Willpower'] * PsionicTalent;
  }

  private loadAdvantages(json: any, model: Gurps, cs: GurpsCharacter) {
    let advantagesJson = json.char_advs
    if (advantagesJson) {

      for (const [key, raw] of Object.entries(advantagesJson)) {
        let value: number[] = raw as number[]

        let cls = "AdvantageValue"
        let advMeta = model.advantages.get(key)
        if (!advMeta) {
          throw new Error(`Advantage '${key}' is not found in db`)
        }

        let advantageValue = new AdvantageValue(advMeta,
          assignNumber(value[0], cls, "[0]/timesTaken"),
        )

        cs.advantages.set(key, advantageValue)

        advMeta.modifiers.forEach(mod => {
          cs.modifiers.push(mod.clone())
        })

      }
    }
  }

}

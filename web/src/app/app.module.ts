import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {AppComponent} from "./app.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MainRollerComponent} from "./ui/main-roller/main-roller.component";
import {MatSliderModule} from "@angular/material/slider";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatTabsModule} from "@angular/material/tabs";
import {HttpClientModule} from "@angular/common/http";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {IconModule} from "@visurel/iconify-angular";
import {CharsheetModule} from "./ui/charsheet/charsheet.module";
import {CreateUserComponent} from "./ui/admin/create-user/create-user.component";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatTooltipModule} from "@angular/material/tooltip";
import {AdminComponent} from "./ui/admin/admin/admin.component";
import {LoginComponent} from "./ui/login/login.component";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {RollsModule} from "./ui/rolls/rolls/rolls.module";
import {MatSelectModule} from "@angular/material/select";
import {CampaignSettingsComponent} from "./ui/admin/campaign-settings/campaign-settings.component";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatExpansionModule} from "@angular/material/expansion";

@NgModule({
  declarations: [
    AppComponent,
    MainRollerComponent,
    CreateUserComponent,
    AdminComponent,
    LoginComponent,
    CampaignSettingsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatCardModule,
    MatButtonModule,
    MatTabsModule,
    HttpClientModule,
    MatListModule,
    MatIconModule,
    MatTableModule,
    IconModule,
    MatTableModule,
    CharsheetModule,
    RollsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatSelectModule,
    MatSnackBarModule,
    MatExpansionModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

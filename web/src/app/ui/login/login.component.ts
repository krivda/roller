import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {AuthenticatorService, Database} from "../../services/authenticator/authenticator.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import icVisibility from "@iconify/icons-ic/twotone-visibility";
import icVisibilityOff from "@iconify/icons-ic/twotone-visibility-off";
import {DocLoaderService} from "../../services/doc-loader/doc-loader.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public inputType = "password";
  public visible = false;

  public icVisibility = icVisibility;
  public icVisibilityOff = icVisibilityOff;

  databases: Database[] = []
  //   [
  //   {name: "local", id: "local"},
  //   {name: "firebase/testchars1", id: "testchars1"},
  //   {name: "Час пламени", id: "shehab1"},
  // ];

  form: FormGroup = this.fb.group({
    login: ["", Validators.required],
    password: ["", Validators.required],
    campaign: [null, Validators.required],
  });

  constructor(public authService: AuthenticatorService,
              private docLoaderService: DocLoaderService,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef) {
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = "password";
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = "text";
      this.visible = true;
      this.cd.markForCheck();
    }
  }

  logon() {

    if (!this.form.valid) {
      this.form.markAllAsTouched()
    } else {
      let login = this.form.get("login")?.value.toLowerCase()
      let password = this.form.get("password")?.value
      let campaign = this.form.get("campaign")?.value

      this.authService.authenticateByPassword(login, password, campaign.id)
    }
  }

  ngOnInit(): void {
    this.docLoaderService.loadJsonAsset("campaigns").subscribe(x => {
        this.databases = x
        if (x.length > 1) {
          this.form.get("campaign")?.setValue(x[0])
        }
      }
    )
  }
}

import {Component} from "@angular/core";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AppUser, AuthenticatorService, Player} from "../../../services/authenticator/authenticator.service";
import {ApiSecService} from "../../../services/api-sec/api-sec.service";
import {DocLoaderService} from "../../../services/doc-loader/doc-loader.service";
import restoreIcon from "@iconify/icons-mdi/restore";
import {mapToPairsArray} from "../../../../code/lib/assigns";
import {Pair} from "../../../../code/lib/pair";
import {CryptoService} from "../../../services/crypto/crypto.service";
import {map, tap} from "rxjs";

class DeForm {
  constructor(public outputAsCampaign: AbstractControl,
              public campaignId: AbstractControl,
              public campaignName: AbstractControl,
              public database: AbstractControl,
              public dmMasterKey: AbstractControl,
              public apiKey: AbstractControl,
              public firebase: AbstractControl,
              public kafka: AbstractControl,
              public playerLogin: AbstractControl,
              public playerPassword: AbstractControl,
              public playerPersonalKey: AbstractControl,
              public isDm: AbstractControl,
              public playerCharacters: AbstractControl,
              public json: AbstractControl,
              public jsonC: AbstractControl
  ) {
  }
}

@Component({
  selector: "app-campaign-settings",
  templateUrl: "./campaign-settings.component.html",
  styleUrls: ["./campaign-settings.component.css"]
})
export class CampaignSettingsComponent {
  restoreIcon = restoreIcon
  form: FormGroup = this.fb.group({
    outputAsCampaign: [false],
    campaignId: [""],
    campaignName: [""],
    database: [""],
    dmMasterKey: [""],
    apiKey: [""],
    firebase: [""],
    kafka: [""],

    playerLogin: ["", Validators.required],
    playerPassword: ["", Validators.required],
    playerPersonalKey: ["", Validators.required],
    isDm: [false, Validators.required],
    playerCharacters: ["", Validators.required],

    json: [""],
    jsonC: [""]
  });

  validErrors: string = ""

  public deForm!: DeForm
  private campaignJsonValue = "json для публикации";

  constructor(private authService: AuthenticatorService,
              private docLoaderService: DocLoaderService,
              private cryptoService: CryptoService,
              private apiSec: ApiSecService,
              private fb: FormBuilder
  ) {

    this.deForm = new DeForm(
      this.form.get("outputAsCampaign")!,
      this.form.get("campaignId")!,
      this.form.get("campaignName")!,
      this.form.get("database")!,
      this.form.get("dmMasterKey")!,
      this.form.get("apiKey")!,
      this.form.get("firebase")!,
      this.form.get("kafka")!,
      this.form.get("playerLogin")!,
      this.form.get("playerPassword")!,
      this.form.get("playerPersonalKey")!,
      this.form.get("isDm")!,
      this.form.get("playerCharacters")!,
      this.form.get("json")!,
      this.form.get("jsonC")!
    )
  }

  encode() {

    this.docLoaderService.loadJsonAsset("auth").pipe(
      map(json => json),
      map(users => {
        const user = (users as AppUser[]).find(u => u.login === this.deForm.playerLogin.value)
        if (!user) {
          this.deForm.playerLogin.setErrors({notfound: "Такого игока нет в базе!"})
          throw new Error(`player ${this.deForm.playerLogin.value} not found`)
        }
        return user as AppUser
      }),
      tap(user => {
        if (this.cryptoService.SHAHash(this.deForm.playerPassword.value) !== user.pwdHash) {
          this.deForm.playerPassword.setErrors({invalid: "Невереный пароль!"})
          throw new Error(`player ${this.deForm.playerLogin.value} password invalid`)

        }
      })
    ).subscribe(_ => {
        if (this.deForm.outputAsCampaign.value) {

          const jsonCamp = new CampaignExport()

          jsonCamp.campaignId = this.deForm.campaignId.value
          jsonCamp.campaignName = this.deForm.campaignName.value
          jsonCamp.database = this.deForm.database.value
          jsonCamp.dmKeyCheck = this.cryptoService.encryptMessage(jsonCamp.campaignId, this.deForm.dmMasterKey.value)

          const apiKey = this.deForm.apiKey.value
          if (apiKey) {
            let entries = new Map<string, string>([
              [this.apiSec.FIREBASE_KEY, this.deForm.firebase.value],
              [this.apiSec.KAFKA_KEY, this.deForm.kafka.value],
            ])

            jsonCamp.apiSec = mapToPairsArray(this.apiSec.serializeSecrets(apiKey, entries))
          }

          const player = this.encodePlayer()
          jsonCamp.players.push(player)

          this.deForm.json.setValue(JSON.stringify(jsonCamp, null, 2))

          this.campaignJsonValue = `сохрани этот файл в campaigns/${jsonCamp.campaignId}.json`

          this.deForm.jsonC.setValue(JSON.stringify({id: jsonCamp.campaignId, name: jsonCamp.campaignName}))

        } else {
          const player = this.encodePlayer()
          this.deForm.json.setValue(JSON.stringify(player, null, 2))
        }
      }
    )
  }

  show() {

    let user = this.authService.loggedUser
    let campaign = user?.campaign!
    let player = campaign.player!

    this.deForm.campaignId.setValue(campaign.campaignId)
    this.deForm.campaignName.setValue(campaign.campaignName)
    this.deForm.database.setValue(campaign.database)
    this.deForm.dmMasterKey.setValue(player.dmKey)
    this.deForm.apiKey.setValue(player.apiKey)

    this.deForm.playerLogin.setValue(player.login)
    this.deForm.isDm.setValue(false)
    this.deForm.playerCharacters.setValue(player.characters.join("\n"))

    this.deForm.firebase.setValue(JSON.stringify(this.apiSec.secMap.get(this.apiSec.FIREBASE_KEY), null, 2))
    this.deForm.kafka.setValue(JSON.stringify(this.apiSec.secMap.get(this.apiSec.KAFKA_KEY), null, 2))

    this.form.markAllAsTouched()
  }

  public generateApiKey() {
    this.deForm.apiKey.setValue(this.pwdGen(10))
  }

  public generatePlayerKey() {
    this.deForm.playerPersonalKey.setValue(this.pwdGen(10))
  }

  public generateDmKey() {
    this.deForm.dmMasterKey.setValue(this.pwdGen(10))
  }

  onAsCampaignChange() {
    if (this.deForm.outputAsCampaign.value) {
      this.deForm.campaignName.setValidators(Validators.required)
      this.deForm.campaignId.setValidators(Validators.required)
      this.deForm.database.setValidators(Validators.required)

      this.deForm.apiKey.setValidators(Validators.required)

      this.deForm.dmMasterKey.setValidators(Validators.required)
      this.deForm.firebase.setValidators(Validators.required)
      this.deForm.kafka.setValidators(Validators.required)
    } else {
      this.deForm.campaignName.setValidators(null)
      this.deForm.campaignId.setValidators(null)
      this.deForm.database.setValidators(null)

      this.deForm.apiKey.setValidators(null)

      this.deForm.dmMasterKey.setValidators(null)
      this.deForm.firebase.setValidators(null)
      this.deForm.kafka.setValidators(null)
    }

    this.campaignJsonValue = "json для публикации";

    this.onIsDmChange()
    this.forceValidation()
  }

  public onIsDmChange() {
    if (this.deForm.isDm.value) {
      this.deForm.dmMasterKey.setValidators(Validators.required)
    } else {
      this.deForm.dmMasterKey.setValidators(this.deForm.campaignId.validator) // same as campaignId (based on campaingId)
    }
    this.forceValidation()
  }

  passwordError(): string | null {
    if (this.deForm.playerPassword.hasError("invalid")) {
      return "Неверный пароль игрока"
    }

    return "";
  }

  loginError() {
    if (this.deForm.playerLogin.hasError("notfound")) {
      return "Игрок с таким логином не найден"
    }

    return "";
  }

  private encodePlayer(): Player {
    let player = new Player()

    player.apiKey = this.cryptoService.encryptMessage(this.deForm.apiKey.value, this.deForm.playerPassword.value)
    player.personalKey = this.cryptoService.encryptMessage(this.deForm.apiKey.value, this.deForm.playerPassword.value)
    player.characters = (this.deForm.playerCharacters.value as string).split("\n")
    player.login = this.deForm.playerLogin.value

    if (this.deForm.isDm.value) {
      player.dmKey = this.cryptoService.encryptMessage(this.deForm.dmMasterKey.value, this.deForm.playerPassword.value)
    } else {
      player.dmKey = ""
    }

    return player
  }

  private pwdGen(length: number): string {
    let result = "";
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@%^*!";
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
  }

  private forceValidation() {
    Object.keys(this.form.controls).forEach(field => {
      const control = this.form.get(field)
      control?.updateValueAndValidity()
      control?.markAsTouched()
    });
  }

}

export class CampaignExport {
  campaignId: string = ""
  campaignName: string = ""
  database: string = ""
  dmKeyCheck: string = "" // защифрованная на DmKey campaignId
  apiSec: Pair<string, any>[] = []
  players: Player[] = []
}

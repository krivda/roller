import {ChangeDetectorRef, Component} from "@angular/core";
import {AbstractControlOptions, FormBuilder, FormGroup, Validators} from "@angular/forms";
import icVisibility from "@iconify/icons-ic/twotone-visibility";
import icVisibilityOff from "@iconify/icons-ic/twotone-visibility-off";
import {ValidatePassword} from "../../../../code/lib/validators/match-pwd.validator";
import {AuthenticatorService} from "../../../services/authenticator/authenticator.service";

@Component({
  selector: "app-create-user",
  templateUrl: "./create-user.component.html",
  styleUrls: ["./create-user.component.css"]
})
export class CreateUserComponent {

  public inputType = "password";
  public visible = false;

  public icVisibility = icVisibility;
  public icVisibilityOff = icVisibilityOff;

  form: FormGroup = this.fb.group({
    player: ["", Validators.required],
    login: ["", Validators.required],
    password: ["", Validators.required],
    confirmPassword: ["", Validators.required],
    json: [""],
  }, {validators: [ValidatePassword.matchPasswords]} as AbstractControlOptions);

  constructor(private authService: AuthenticatorService,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef) {
  }

  encode() {

    let player = this.form.get("player")?.value
    let login = this.form.get("login")?.value.toLowerCase()
    let password = this.form.get("password")?.value

    let json = this.authService.serializeUserInfo(player, login, password)

    this.form.get("json")?.setValue(json)

  }

  // decrypt() {
  //   this.userName.setValue(this.cryptoService.AESDecrypt(this.encrypted.value ?? "", "kokojumbo"))
  //   this.encrypted.setValue("")
  //
  // }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = "password";
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = "text";
      this.visible = true;
      this.cd.markForCheck();
    }
  }
}

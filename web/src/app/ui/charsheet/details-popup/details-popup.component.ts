import {Component, Input, OnInit} from "@angular/core";
import {ModifiableValue} from "../../../../code/model/gurps/gurps-character";
import {CharModifier} from "../../../../code/model/gurps/char-modifier";
import {signify} from "../../../../code/lib/text";

@Component({
  selector: "app-details-popup",
  templateUrl: "./details-popup.component.html",
  styleUrls: ["./details-popup.component.css"]
})
export class DetailsPopupComponent implements OnInit {

  public enabledMods: CharModifier[] = []

  @Input() value!: ModifiableValue;
  public hasBaseMods: boolean = false;
  public changedByDisplayValue: string = "";

  ngOnInit(): void {
    this.value.appliedModifiers
      .filter(m => m.enabled)
      .forEach(m => this.enabledMods.push(m))

    let baseMods = this.value.componentsValues
      .flatMap(base => base.appliedModifiers)
      .filter(m => m.enabled)

    if (baseMods.length > 0) {
      this.hasBaseMods = true
    }

    this.changedByDisplayValue = signify(this.value.changedBy)
  }
}

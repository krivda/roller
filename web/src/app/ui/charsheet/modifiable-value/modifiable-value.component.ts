import {Component, Input} from "@angular/core";
import {ModifiableValue} from "../../../../code/model/gurps/gurps-character";
import commentQuestionOutline from "@iconify/icons-mdi/comment-question-outline";


@Component({
  selector: "app-modifiable-value",
  templateUrl: "./modifiable-value.component.html",
  styleUrls: ["./modifiable-value.component.css"]
})
export class ModifiableValueComponent {

  @Input() value!: ModifiableValue;
  public commentQuestionOutline = commentQuestionOutline
  isOpen: boolean = false;


  show() {
    this.isOpen = true
  }
}

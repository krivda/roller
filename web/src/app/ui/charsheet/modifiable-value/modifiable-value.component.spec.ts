import {ComponentFixture, TestBed} from "@angular/core/testing";

import {ModifiableValueComponent} from "./modifiable-value.component";

describe("ModifiableValueComponent", () => {
  let component: ModifiableValueComponent;
  let fixture: ComponentFixture<ModifiableValueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModifiableValueComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ModifiableValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

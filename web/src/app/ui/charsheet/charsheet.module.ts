import {NgModule} from "@angular/core";
import {SkillsListComponent} from "./skills-list/skills-list.component";
import {ModifiableValueComponent} from "./modifiable-value/modifiable-value.component";
import {AttributesComponent} from "./attributes/attributes.component";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatSliderModule} from "@angular/material/slider";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatTabsModule} from "@angular/material/tabs";
import {HttpClientModule} from "@angular/common/http";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {IconModule} from "@visurel/iconify-angular";
import {CharsheetComponent} from "./charsheet/charsheet.component";
import {SecAttributesComponent} from "./sec-attributes/sec-attributes.component";
import {CountersComponent} from "./counters/counters.component";
import {DefencesComponent} from "./defences/defences.component";
import {AdvantagesComponent} from "./advantages/advantages.component";
import {ModifiersComponent} from "./modifiers/modifiers.component";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {WeaponsComponent} from "./weapons/weapons.component";
import {HeaderComponent} from "./header/header.component";
import {OverlayModule} from "@angular/cdk/overlay";
import {A11yModule} from "@angular/cdk/a11y";
import {DetailsPopupComponent} from "./details-popup/details-popup.component";
import {CheckRollDialogComponent} from "./check-roll-dialog/check-roll-dialog.component";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {FireControlComponent} from "./fire-control/fire-control.component";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatTooltipModule} from "@angular/material/tooltip";


@NgModule({
  declarations: [
    SkillsListComponent,
    ModifiableValueComponent,
    AttributesComponent,
    CharsheetComponent,
    SecAttributesComponent,
    CountersComponent,
    DefencesComponent,
    AdvantagesComponent,
    ModifiersComponent,
    WeaponsComponent,
    HeaderComponent,
    DetailsPopupComponent,
    CheckRollDialogComponent,
    FireControlComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatCardModule,
    MatButtonModule,
    MatTabsModule,
    HttpClientModule,
    MatListModule,
    MatIconModule,
    MatTableModule,
    IconModule,
    MatTableModule,
    MatCheckboxModule,
    OverlayModule,
    A11yModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    MatProgressBarModule,
    MatTooltipModule
  ],
  exports: [
    CharsheetComponent
  ]
})
export class CharsheetModule {
}

import {Component, Input, OnInit} from "@angular/core";
import {CounterValue, GurpsCharacter} from "../../../../code/model/gurps/gurps-character";
import diceIcon from "@iconify/icons-fa-solid/dice";

@Component({
  selector: "app-sec-attributes",
  templateUrl: "./sec-attributes.component.html",
  styleUrls: ["./sec-attributes.component.css"]
})
export class SecAttributesComponent implements OnInit {

  @Input() public character!: GurpsCharacter;

  public diceIcon = diceIcon

  public speed!: CounterValue
  public move!: CounterValue

  constructor() {
  }

  ngOnInit(): void {

    this.speed = this.character.counters.get("Speed")!
    this.move = this.character.counters.get("Move")!
    // this.attributes.push(this.character.secondaryAttributes.get("Perception")!)
    // this.attributes.push(this.character.secondaryAttributes.get("will")!)
    // this.attributes.push(this.character.secondaryAttributes.get("HP")!)
  }

}

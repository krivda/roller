import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SecAttributesComponent} from "./sec-attributes.component";

describe("SecAttributesComponent", () => {
  let component: SecAttributesComponent;
  let fixture: ComponentFixture<SecAttributesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SecAttributesComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(SecAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

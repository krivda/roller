import {ComponentFixture, TestBed} from "@angular/core/testing";

import {FireControlComponent} from "./fire-control.component";

describe("FireControlComponent", () => {
  let component: FireControlComponent;
  let fixture: ComponentFixture<FireControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FireControlComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(FireControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnChanges, OnInit, SimpleChanges} from "@angular/core";
import {EquippedWeapon, GurpsCharacter, SkillValue} from "../../../../code/model/gurps/gurps-character";
import {FormControl} from "@angular/forms";
import restoreIcon from "@iconify/icons-mdi/restore";
import {RollsService} from "../../../services/rolls/rolls.service";
import {Gurps} from "../../../../code/model/gurps/gurps";
import {signify} from "../../../../code/lib/text";
import {OutcomeKind} from "../../../../code/model/gurps/rolls/checkRoll";

@Component({
  selector: "app-fire-control",
  templateUrl: "./fire-control.component.html",
  styleUrls: ["./fire-control.component.css"]
})
export class FireControlComponent implements OnChanges, OnInit {

  restoreIcon = restoreIcon

  public MAX_AIM_ROUNDS = Gurps.MAX_AIM_ROUNDS

  @Input() character!: GurpsCharacter;
  @Input() weapon!: EquippedWeapon;

  public rangeControl = new FormControl(2)
  public rangeMod: number = 0
  public rangeModText: string = "";

  public sizeControl = new FormControl(2)
  public sizeMod: number = 0
  public sizeModText: string = "";

  public speedControl = new FormControl(0)
  public speedMod: number = 0
  public speedModText: string = signify(this.speedMod);

  public additionalDifficultyControl = new FormControl(0);

  public targetMod: number = 0
  public targetModText = signify(this.targetMod)

  public targetDRControl = new FormControl(0);
  public targetNameControl = new FormControl("");

  public targetDamageDealtControl = new FormControl(0);

  public shotsFiredControl = new FormControl<number>(0);
  public shotsFiredMax = 0;
  public recoilMod: number = 0
  public recoilModText: string = "";


  public aimRoundsControl = new FormControl<number>(0);
  public aimingMod: number = 0
  public aimingModText: string = "0";
  public canAimHint: string = "";
  public canAim: boolean = true


  gunner: SkillValue | null = null;

  public ammo: number = 1;
  public clipSize: number = 0;
  public spentAmmoProgress: number = 100;

  public snapshot: number = 0
  public canFire: boolean = true;
  public totalDifficulty: number = 0;
  public canFireHint: string = "";
  private allMods: number = 0;

  constructor(private rollingService: RollsService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.shotsFiredMax = this.weapon.meta.rof1
    this.reload()
    this.calcShotDifficulty()
  }

  ngOnInit(): void {

    this.gunner = this.character.skills.get("Gunner")!
    this.shotsFiredMax = this.weapon.meta.rof1
    this.reload()
    this.calcShotDifficulty()
  }

  changeShotsFired() {
    this.applyShotsFired(this.shotsFiredControl.value ?? 0)
  }

  resetRecoil() {
    this.applyShotsFired(0)
  }

  changeAimRounds() {
    this.applyAimRounds(this.aimRoundsControl.value ?? 0)
  }

  resetAiming() {
    this.applyAimRounds(0)
  }

  calcShotDifficulty() {

    //summarize target mods
    const range = this.rangeControl.value ?? 0
    const size = this.sizeControl.value ?? 0
    const speed = this.speedControl.value ?? 0

    this.rangeMod = Gurps.calcShootMod(range)
    this.rangeModText = signify(this.rangeMod)
    this.sizeMod = -Gurps.calcShootMod(size)
    this.sizeModText = signify(this.sizeMod)
    this.speedMod = Gurps.calcShootMod(speed, true)
    this.speedModText = signify(this.speedMod)
    this.targetMod = this.rangeMod + this.sizeMod + this.speedMod + (this.additionalDifficultyControl.value ?? 0)
    this.targetModText = signify(this.targetMod)

    //summarize shooter mods
    const weapon = this.weapon
    const skill = this.gunner?.cached!

    const shotsFired = this.shotsFiredControl.value ?? 0
    this.recoilMod = -shotsFired * weapon.meta.recoil
    this.recoilModText = signify(this.recoilMod)

    const aimRounds = this.aimRoundsControl.value ?? 0
    const aimBonus = Math.min(skill, weapon.meta.acc)
    this.aimingMod = aimRounds * aimBonus
    this.aimingModText = signify(this.aimingMod)

    this.allMods = this.targetMod + this.recoilMod + this.aimingMod
    //apply snapshot
    if (aimRounds === 0 && skill + this.allMods < weapon.meta.ss) {
      this.snapshot = Gurps.SNAPSHOT_MODIFIER
    } else {
      this.snapshot = 0
    }

    this.allMods += this.snapshot

    this.totalDifficulty = skill + this.allMods

    if (this.totalDifficulty < 0) {
      this.totalDifficulty = 0
    }
    if (this.totalDifficulty > 18) {
      this.totalDifficulty = 18
    }
    if (aimRounds === Gurps.MAX_AIM_ROUNDS) {
      this.canAim = false
      this.canAimHint = "Max aiming round reached"
    } else {
      this.canAim = true
      this.canAimHint = ""
    }

    if (this.ammo === 0 || this.shotsFiredMax === shotsFired) {
      this.canFire = false

      if (this.ammo === 0) {
        this.canFireHint = "Out of ammo!"
      } else {
        this.canFireHint = "ROF exceeded!"
      }

    } else {
      this.canFire = true
      this.canFireHint = ""
    }
  }

  doAim() {
    this.applyAimRounds((this.aimRoundsControl.value ?? 0) + 1)
  }

  shoot() {

    let targetName = ""
    if (this.targetNameControl.value) {
      targetName = ` at ${this.targetNameControl.value}`
    }

    //roll to hit dice!
    const checkRoll = this.rollingService.makeCheckRoll(this.gunner!, this.character, this.allMods,
      `shoots ${this.weapon.meta.name}${targetName}`, false)

    //increase recoil
    this.shotsFiredControl.setValue((this.shotsFiredControl.value ?? 0) + 1)

    //decrease ammo
    this.ammo = this.ammo - this.weapon.meta.rof2

    if (this.ammo < 0) this.ammo = 0
    this.spentAmmoProgress = (this.ammo / this.clipSize) * 100

    //get damage dealt

    if (checkRoll.result.outcome) {
      let damageRoll = this.rollingService.makeDamageRoll(
        this.weapon.meta,
        this.character,
        this.targetDRControl.value ?? 0,
        (checkRoll.result.outcomeKind === OutcomeKind.criticalSuccess || checkRoll.result.outcomeKind === OutcomeKind.astonishingSuccess),
        false)
      this.targetDamageDealtControl.setValue((this.targetDamageDealtControl.value ?? 0) + damageRoll.result.sum)
    }


    this.calcShotDifficulty()
  }

  doReset() {
    this.targetDamageDealtControl.setValue(0)
    this.applyAimRounds(0)
  }

  reload() {
    this.clipSize = this.weapon.meta.ammo
    this.ammo = this.clipSize
    this.spentAmmoProgress = (this.ammo / this.clipSize) * 100
    this.targetDamageDealtControl.setValue(0)

    this.doReset()
  }

  resetDamage() {
    this.targetDamageDealtControl.setValue(0)
  }

  private applyShotsFired(number: number) {

    if (number < 0) number = 0
    if (number > this.shotsFiredMax) number = this.shotsFiredMax

    this.shotsFiredControl.setValue(number)
    this.calcShotDifficulty()
  }

  private applyAimRounds(value: number) {

    let rounds = value

    if (value > Gurps.MAX_AIM_ROUNDS) rounds = Gurps.MAX_AIM_ROUNDS
    if (value < 0) rounds = 0

    this.aimRoundsControl.setValue(rounds)
    this.shotsFiredControl.setValue(0)
    this.targetDamageDealtControl.setValue(0)

    this.calcShotDifficulty()
  }
}

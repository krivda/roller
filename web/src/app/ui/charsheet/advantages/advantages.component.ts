import {Component, Input, OnInit} from "@angular/core";
import {GurpsCharacter} from "../../../../code/model/gurps/gurps-character";

@Component({
  selector: "app-advantages",
  templateUrl: "./advantages.component.html",
  styleUrls: ["./advantages.component.css"]
})
export class AdvantagesComponent implements OnInit {

  @Input() character!: GurpsCharacter;

  ngOnInit(): void {
  }

}

import {Component, Input} from "@angular/core";
import {GurpsCharacter} from "../../../../code/model/gurps/gurps-character";
import {CharModifier} from "../../../../code/model/gurps/char-modifier";
import {MatCheckboxChange} from "@angular/material/checkbox";

@Component({
  selector: "app-modifiers",
  templateUrl: "./modifiers.component.html",
  styleUrls: ["./modifiers.component.css"]
})

export class ModifiersComponent {

  @Input() character!: GurpsCharacter;

  enable(mod: CharModifier, $event: MatCheckboxChange) {
    mod.enabled = $event.checked
    this.character.recalculateCharacter()
  }
}

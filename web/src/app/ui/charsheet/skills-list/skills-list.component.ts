import {Component, Input, OnInit} from "@angular/core";
import {AttributeValue, GurpsCharacter, SkillValue} from "../../../../code/model/gurps/gurps-character";
import diceIcon from "@iconify/icons-fa-solid/dice";
import {CheckRollDialogComponent, CheckRollDialogParams} from "../check-roll-dialog/check-roll-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: "app-skills-list",
  templateUrl: "./skills-list.component.html",
  styleUrls: ["./skills-list.component.css"]
})
export class SkillsListComponent implements OnInit {

  constructor(private dialogs: MatDialog) {
  }

  @Input() character!: GurpsCharacter;

  public diceIcon = diceIcon

  public skills: SkillValue[] = []
  displayedColumns: string[] = ["dice", "name", "value", "attr"];


  ngOnInit(): void {
    this.character.skills.forEach(sk => {
      this.skills.push(sk)
    })
  }

  rollValue(attr: AttributeValue) {
    this.dialogs.open(CheckRollDialogComponent, {
        data: new CheckRollDialogParams(attr, this.character)
      }
    )
  }
}

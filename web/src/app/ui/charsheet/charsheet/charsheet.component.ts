import {Component, Input, OnInit} from "@angular/core";
import {GurpsCharacter} from "../../../../code/model/gurps/gurps-character";

@Component({
  selector: "app-charsheet",
  templateUrl: "./charsheet.component.html",
  styleUrls: ["./charsheet.component.css"]
})
export class CharsheetComponent implements OnInit {

  @Input() character!: GurpsCharacter;

  ngOnInit(): void {
  }

}

import {Component, Input, OnInit} from "@angular/core";
import {AttributeValue, CounterValue, GurpsCharacter} from "../../../../code/model/gurps/gurps-character";
import diceIcon from "@iconify/icons-fa-solid/dice";
import {CounterKind} from "../../../../code/model/gurps/gurps-model";
import {CheckRollDialogComponent, CheckRollDialogParams} from "../check-roll-dialog/check-roll-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: "app-counters",
  templateUrl: "./counters.component.html",
  styleUrls: ["./counters.component.css"]
})
export class CountersComponent implements OnInit {

  @Input() public character!: GurpsCharacter;

  public diceIcon = diceIcon
  public eCounterKind = CounterKind;

  public counters: CounterValue[] = []
  public fat!: CounterValue;

  constructor(private dialogs: MatDialog) {
  }

  ngOnInit(): void {

    this.counters.push(this.character.counters.get("Fatigue")!)
    this.counters.push(this.character.counters.get("Perception")!)
    this.counters.push(this.character.counters.get("Willpower")!)
    this.counters.push(this.character.counters.get("Hits")!)
  }

  rollValue(attr: AttributeValue) {
    this.dialogs.open(CheckRollDialogComponent, {
        data: new CheckRollDialogParams(attr, this.character)
      }
    )
  }

}

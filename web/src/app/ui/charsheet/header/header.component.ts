import {Component, Input} from "@angular/core";
import {GurpsCharacter} from "../../../../code/model/gurps/gurps-character";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent {

  @Input() public character!: GurpsCharacter;

}

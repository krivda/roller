import {Component, Input, OnInit} from "@angular/core";
import {EquippedWeapon, GurpsCharacter} from "../../../../code/model/gurps/gurps-character";
import pistolGun from "@iconify/icons-game-icons/pistol-gun";
import {MatButtonToggleChange} from "@angular/material/button-toggle";


@Component({
  selector: "app-weapons",
  templateUrl: "./weapons.component.html",
  styleUrls: ["./weapons.component.css"]
})
export class WeaponsComponent implements OnInit {

  public pistolGun = pistolGun

  public weapons: EquippedWeapon[] = []
  displayedColumns: string[] = ["fire", "weapon", "dmgType", "damage", "ROF", "SS", "ACC", "recoil", "range", "ammo"];


  @Input() character!: GurpsCharacter;

  ngOnInit(): void {
    this.character.weapons.forEach(w => {
      this.weapons.push(w)
    })
  }

  equip(weapon: EquippedWeapon, $event: MatButtonToggleChange) {
    if ($event.source.checked) {
      this.character.activeWeapon = weapon
    } else {
      this.character.activeWeapon = null
    }
  }
}

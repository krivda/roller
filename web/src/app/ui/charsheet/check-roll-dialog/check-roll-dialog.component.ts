import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {GurpsCharacter, ModifiableValue} from "../../../../code/model/gurps/gurps-character";
import {FormControl} from "@angular/forms";
import {MatSliderChange} from "@angular/material/slider";
import {RollsService} from "../../../services/rolls/rolls.service";

export class CheckRollDialogParams {
  constructor(public value: ModifiableValue,
              public character: GurpsCharacter) {
  }
}

@Component({
  selector: "app-check-roll-dialog",
  templateUrl: "./check-roll-dialog.component.html",
  styleUrls: ["./check-roll-dialog.component.css"]
})
export class CheckRollDialogComponent {

  difficultyInput = new FormControl(0)
  difficultySlider = new FormControl(0)
  rollText = new FormControl("");
  secretRollCheckbox = new FormControl(false);

  constructor(
    private rollRoll: RollsService,
    public dialogRef: MatDialogRef<CheckRollDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public params: CheckRollDialogParams
  ) {
  }

  roll() {
    this.rollRoll.makeCheckRoll(
      this.params.value, this.params.character,
      this.difficultyInput.value!, this.rollText.value ?? "",
      this.secretRollCheckbox.value!)
    this.dialogRef.close()
  }

  onSlide($event: MatSliderChange) {
    this.difficultyInput.setValue($event.value)
  }

  valueEntered() {
    this.difficultySlider.setValue(this.difficultyInput.value)
  }
}

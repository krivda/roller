import {ComponentFixture, TestBed} from "@angular/core/testing";

import {CheckRollDialogComponent} from "./check-roll-dialog.component";

describe("CheckRollDialogComponent", () => {
  let component: CheckRollDialogComponent;
  let fixture: ComponentFixture<CheckRollDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CheckRollDialogComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(CheckRollDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

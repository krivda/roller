import {Component, Input, OnInit} from "@angular/core";
import {AttributeValue, CounterValue, EquippedArmor, GurpsCharacter} from "../../../../code/model/gurps/gurps-character";
import diceIcon from "@iconify/icons-fa-solid/dice";
import {CounterKind} from "../../../../code/model/gurps/gurps-model";
import {CheckRollDialogComponent, CheckRollDialogParams} from "../check-roll-dialog/check-roll-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: "app-defences",
  templateUrl: "./defences.component.html",
  styleUrls: ["./defences.component.css"]
})
export class DefencesComponent implements OnInit {

  public diceIcon = diceIcon
  public eCounterKind = CounterKind;


  @Input() public character!: GurpsCharacter;
  public armor!: EquippedArmor;
  public armorHp!: CounterValue;
  public armorMalf!: CounterValue;

  public defences: CounterValue[] = [];

  constructor(
    private dialogs: MatDialog) {

  }

  ngOnInit(): void {

    this.armor = this.character.armor
    this.armorHp = this.character.counters.get("armorHP")!
    this.armorMalf = this.character.counters.get("Malf")!

    this.defences.push(this.character.counters.get("Dodge")!)
    this.defences.push(this.character.counters.get("PD")!)
    this.defences.push(this.character.counters.get("DR")!)
    this.defences.push(this.character.counters.get("RR")!)

  }

  rollValue(attr: AttributeValue) {
    this.dialogs.open(CheckRollDialogComponent, {
        data: new CheckRollDialogParams(attr, this.character)
      }
    )
  }

}

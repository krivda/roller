import {ComponentFixture, TestBed} from "@angular/core/testing";

import {DefencesComponent} from "./defences.component";

describe("DefencesComponent", () => {
  let component: DefencesComponent;
  let fixture: ComponentFixture<DefencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DefencesComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(DefencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

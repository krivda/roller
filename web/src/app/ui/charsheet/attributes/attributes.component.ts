import {Component, Input} from "@angular/core";
import {AttributeValue, GurpsCharacter} from "../../../../code/model/gurps/gurps-character";
import diceIcon from "@iconify/icons-fa-solid/dice";
import {MatDialog} from "@angular/material/dialog";
import {CheckRollDialogComponent, CheckRollDialogParams} from "../check-roll-dialog/check-roll-dialog.component";

@Component({
  selector: "app-attributes",
  templateUrl: "./attributes.component.html",
  styleUrls: ["./attributes.component.css"]
})
export class AttributesComponent {

  @Input() character!: GurpsCharacter;

  public diceIcon = diceIcon

  constructor(
    private dialogs: MatDialog) {
  }

  rollValue(attr: AttributeValue) {
    this.dialogs.open(CheckRollDialogComponent, {
        data: new CheckRollDialogParams(attr, this.character)
      }
    )
  }
}

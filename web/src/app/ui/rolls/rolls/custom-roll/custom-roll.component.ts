import {Component} from "@angular/core";
import {FormControl} from "@angular/forms";
import {RollsService} from "../../../../services/rolls/rolls.service";
import {MessageService} from "../../../../services/message/message.service";

@Component({
  selector: "app-custom-roll",
  templateUrl: "./custom-roll.component.html",
  styleUrls: ["./custom-roll.component.css"]
})
export class CustomRollComponent {

  expressionControl = new FormControl("")

  constructor(private rollService: RollsService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
  }

  evaluate() {
    let res
    try {
      res = this.rollService.evalExpr(this.expressionControl.value!)

    } catch (err) {
      if (err instanceof Error) {
        this.messageService.publishMessage(err.message)
      } else {
        this.messageService.publishMessage(`${err}`)
      }
      return
    }

    this.messageService.publishMessage(`Результат = ${res}`)
  }
}

import {ComponentFixture, TestBed} from "@angular/core/testing";

import {CustomRollComponent} from "./custom-roll.component";

describe("CustomRollComponent", () => {
  let component: CustomRollComponent;
  let fixture: ComponentFixture<CustomRollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomRollComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(CustomRollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

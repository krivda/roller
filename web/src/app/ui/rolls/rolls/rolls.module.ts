import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {EventLogComponent} from "./event-log/event-log.component";
import {RollsComponent} from "./rolls/rolls.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {CustomRollComponent} from "./custom-roll/custom-roll.component";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatListModule} from "@angular/material/list";


@NgModule({
  declarations: [
    EventLogComponent,
    RollsComponent,
    CustomRollComponent,
  ],
  exports: [
    RollsComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatListModule
  ]
})
export class RollsModule {
}

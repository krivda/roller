import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {RollsService} from "../../../../services/rolls/rolls.service";
import {FormControl} from "@angular/forms";
import {Roll, RollType} from "../../../../../code/model/gurps/rolls";
import {MessageService} from "../../../../services/message/message.service";
import {CheckRoll} from "../../../../../code/model/gurps/rolls/checkRoll";
import {DamageRoll} from "../../../../../code/model/gurps/rolls/damageRoll";

@Component({
  selector: "app-event-log",
  templateUrl: "./event-log.component.html",
  styleUrls: ["./event-log.component.css"]
})
export class EventLogComponent implements OnInit, AfterViewChecked {


  @ViewChild("rollContainer")
  rollContainer!: ElementRef

  public rollsTextControl = new FormControl("");

  public rollHistory: Roll[] = []

  constructor(
    private rollsService: RollsService,
    private messageService: MessageService) {
  }

  ngAfterViewChecked(): void {
    this.rollContainer.nativeElement.scrollTop = this.rollContainer.nativeElement.scrollHeight
  }

  ngOnInit(): void {
    this.rollsService.getRollsUpdater()
      .subscribe(roll => {

        if (roll.rollType === RollType.checkRoll) {
          this.outputCheckRoll(roll as CheckRoll)
        } else if (roll.rollType === RollType.damageRoll) {
          this.outputDamageRoll(roll as DamageRoll)
        }

        this.rollHistory.push(roll)
      })

    this.messageService.getMessagesUpdater()
      .subscribe(m => {
        this.appendLog(m)
      })
  }

  private appendLog(message: String) {
    this.rollsTextControl.setValue(this.rollsTextControl.value! + message + "\n")
  }

  castToCheckRoll(roll: Roll): CheckRoll | null {
    if (roll.rollType === RollType.checkRoll) return roll as CheckRoll
    return null
  }

  castToDamageRoll(roll: Roll): DamageRoll | null {
    if (roll.rollType === RollType.damageRoll) return roll as DamageRoll
    return null
  }

  private outputCheckRoll(roll: CheckRoll) {

    let rollBrief = `${roll.character} ${roll.description} (${roll.subject}) \
         \n[${roll.result.dices.join(", ")}] = ${roll.result.sum} против сложности ${roll.totalDifficulty} и это ${roll.result.outcomeText}`

    this.appendLog(rollBrief)
  }

  private outputDamageRoll(roll: DamageRoll) {
    let rollBrief = `${roll.character} наносит урон из ${roll.description} \
         \n([${roll.result.dices.join(", ")}] + ${roll.addendum} - ${roll.DR}) * ${roll.multiplier} = ${roll.result.sum}`

    this.appendLog(rollBrief)
  }
}

import {ChangeDetectorRef, Component, OnDestroy, OnInit} from "@angular/core";
import {CharacterLoaderService} from "../../services/character/character-loader.service";
import {GurpsCharacter} from "../../../code/model/gurps/gurps-character";
import {AuthenticatorService} from "../../services/authenticator/authenticator.service";
import {MatTabChangeEvent} from "@angular/material/tabs";
import {Title} from "@angular/platform-browser";
import settingsIcon from "@iconify/icons-material-symbols/settings";
import {delay, filter, of, Subject, switchMap, takeUntil} from "rxjs";
import {ApiSecService} from "../../services/api-sec/api-sec.service";
import {FirebaseService} from "../../services/firebase/firebase.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: "app-main-roller",
  templateUrl: "./main-roller.component.html",
  styleUrls: ["./main-roller.component.css"]
})
export class MainRollerComponent implements OnInit, OnDestroy {

  public characters: GurpsCharacter[] = []
  public canShowSettings: boolean = false

  settingsIcon = settingsIcon

  private onDestroy$: Subject<void> = new Subject<void>();


  constructor(
    public authService: AuthenticatorService,
    private characterService: CharacterLoaderService,
    private apiSecService: ApiSecService,
    private firebaseService: FirebaseService,
    private snackBar: MatSnackBar,
    private titleService: Title,
    private cd: ChangeDetectorRef
  ) {
  }

  ngOnInit(): void {

    if (!this.authService.loggedUser) {
      throw new Error("No authentication!!!")
    }

    this.authService.isAuthenticated$.pipe(
      filter(v => v), // fire if auth changed
      switchMap(() => this.characterService.loadAll(this.authService.loggedUser!)),
      takeUntil(this.onDestroy$)
    ).subscribe(
      c => {
        let existingIndex = this.characters.findIndex(char => char.name == c.name)
        if (existingIndex != -1) {
          this.characters[existingIndex] = c
          this.cd.detectChanges()
          this.snackBar.open(`Character ${c.name} was updated from remote`, "Dismiss", {duration: 1000 * 6});
        } else {
          this.characters.push(c)
        }
      }
    )

    // hack to move settings tab to be last
    of(1).pipe(
      delay(2000)
    ).subscribe(
      () => this.checkCanShowSettings()
    )
  }


  onTabChange($event: MatTabChangeEvent) {
    if ($event.tab.textLabel) {
      this.titleService.setTitle($event.tab.textLabel);
      this.checkCanShowSettings()
    }
  }

  logoff() {
    this.authService.logoff()
    this.firebaseService.logoff()
    this.characters = []
  }

  ngOnDestroy(): void {
    this.onDestroy$.next()
  }

  private checkCanShowSettings() {
    if (this.authService.loggedUser?.campaign.player?.isDM) {
      this.canShowSettings = true
    }
  }

}

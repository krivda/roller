import {ComponentFixture, TestBed} from "@angular/core/testing";

import {Weapons2Component} from "./weapons2.component";

describe("Weapons2Component", () => {
  let component: Weapons2Component;
  let fixture: ComponentFixture<Weapons2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Weapons2Component]
    })
      .compileComponents();

    fixture = TestBed.createComponent(Weapons2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});

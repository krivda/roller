import {Component, Input, OnInit} from "@angular/core";
import {EquippedWeapon, GurpsCharacter} from "../../../../code/model/gurps/gurps-character";
import pistolGun from "@iconify/icons-game-icons/pistol-gun";

@Component({
  selector: "app-weapons2",
  templateUrl: "./weapons2.component.html",
  styleUrls: ["./weapons2.component.css"]
})
export class Weapons2Component implements OnInit {

  public pistolGun = pistolGun

  public weapons: EquippedWeapon[] = []
  displayedColumns: string[] = ["fire", "weapon", "dmgType", "SS", "ROF", "ACC", "recoil", "range", "ammo"];


  @Input() character!: GurpsCharacter;

  ngOnInit(): void {
    this.character.weapons.forEach(w => {
      this.weapons.push(w)
    })
  }
}

import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {Weapons2Component} from "./weapons2/weapons2.component";
import {CheckFlexComponent} from "./check-flex/check-flex.component";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatDividerModule} from "@angular/material/divider";


@NgModule({
  declarations: [
    Weapons2Component,
    CheckFlexComponent

  ],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatDividerModule
  ]
})
export class TestsModule {
}

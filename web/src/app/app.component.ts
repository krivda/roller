import {Component} from "@angular/core";
import {AuthenticatorService} from "./services/authenticator/authenticator.service";


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "GURPS interactive charsheet";


  constructor(public authService: AuthenticatorService) {

  }


}
